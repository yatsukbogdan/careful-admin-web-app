import axios from 'axios';
import { API_URL, NOVA_POSHTA_API_URL } from '../config';

export default class API {
  static init() {
    const api = axios.create({ baseURL: API_URL });
    this.api = api;
  }

  static patch(token) {
    this.api.defaults.headers.Authorization = `Bearer ${token}`;
  }

  static logout() {
    this.api.defaults.headers.Authorization = null;
  }

  static getProductsList(categoryId) {
    if (!categoryId) return this.api.get('/admin/product/list');
    return this.api.get(`/admin/product/list?categoryId=${categoryId}`);
  }

  static login(body) {
    return this.api.post('/user/login', body);
  }
  static getCountriesList() {
    return this.api.get('/country/list');
  }
  static getCitiesList(input) {
    return this.api.get(`/rozetka/cities?name=${input ? input : 'а'}`);
  }
  static getStreetsList(input, cityId) {
    return this.api.get(`/rozetka/streets?cityId=${cityId}&name=${input ? input : 'а'}`);
  }
  static getCategoriesList() {
    return this.api.get('/category/list');
  }
  static getProduct(id) {
    return this.api.get(`/admin/product/${id}`);
  }
  static addCustomer(body) {
    return this.api.post('/customer', body);
  }
  static addInvoice(body) {
    return this.api.post('/invoice', body);
  }
  static addDelivery(body) {
    return this.api.post('/delivery', body);
  }

  static addCategory(body) {
    return this.api.post('/admin/category', body);
  }
  static updateCategory(id, name) {
    return this.api.put(`/admin/category/${id}`, { name });
  }
  static deleteCategory(id) {
    return this.api.delete(`/admin/category/${id}`);
  }

  static addProduct(body) {
    return this.api.post('/admin/product', body);
  }
  static updateProduct(id, body) {
    return this.api.put(`/admin/product/${id}`, body);
  }
  static deleteProduct(id) {
    return this.api.delete(`/admin/product/${id}`);
  }
  static toggleProductFeedbackVisibility(productId, feedbackId) {
    return this.api.get(`/admin/product/${productId}/feedback/${feedbackId}/toggle-visibility`);
  }
  static uploadProductImage(body, id) {
    return this.api.post(`/admin/product/${id}/image`, body, { headers: { 'Content-Type': 'multipart/form-data' } });
  }
  static updateProductAlbumOrder(body, id) {
    return this.api.put(`/admin/product/${id}/album-order`, body);
  }

  static getInvoicesList() {
    return this.api.get('/admin/invoice/list');
  }
  static getDeliveriesList() {
    return this.api.get('/admin/delivery/list');
  }
  static getCustomersList() {
    return this.api.get('/admin/customer/list');
  }

  static addPurchaseInvoice(products) {
    return this.api.post('/admin/invoice', { products, type: 2 });
  }
  static cancelInvoice(id) {
    return this.api.put(`/admin/invoice/${id}/cancel`);
  }
  static updateDeliverySentToCustomerStatus(id, body) {
    return this.api.put(`/admin/delivery/${id}/sent-to-customer`, body);
  }
}

export class NovaPoshtaAPI {
  static init() {
    const api = axios.create({ baseURL: NOVA_POSHTA_API_URL });
    this.api = api;
  }

  static getCities(input) {
    return this.api.post('/Address/getCities', {
      modelName: 'Address',
      calledMethod: 'getCities',
      methodProperties: {
        FindByString: input ? input.trim() : ''
      }
    });
  }

  static getWarehouses(cityId) {
    return this.api.post('/AddressGeneral/getWarehouses', {
      modelName: 'AddressGeneral',
      calledMethod: 'getWarehouses',
      methodProperties: {
        Language: 'ru',
        CityRef: cityId
      }
    });
  }
}
