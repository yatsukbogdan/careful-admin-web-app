import products from './products';
import categories from './categories';
import invoices from './invoices';
import deliveries from './deliveries';
import customers from './customers';
import { combineReducers } from 'redux';

export default combineReducers({
  categories,
  invoices,
  products,
  customers,
  deliveries
});
