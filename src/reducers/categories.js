import { ADD_CATEGORIES, ADD_CATEGORY, UPDATE_CATEGORY, REMOVE_CATEGORY } from '../actions/categories';
import { addItemToState, updateItemInState, removeItemFromState, addItemsToState } from '../utils';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CATEGORY:
      return addItemToState(action.payload.category, state);
    case UPDATE_CATEGORY:
      return updateItemInState(action.payload.category, action.payload.id, state);
    case REMOVE_CATEGORY:
      return removeItemFromState(action.payload.id, state);
    case ADD_CATEGORIES:
      return addItemsToState(action.payload.categories, state);
    default:
      return state;
  }
};
