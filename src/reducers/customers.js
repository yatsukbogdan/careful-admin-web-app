import { ADD_CUSTOMERS } from '../actions/customers';
import { addItemsToState } from '../utils';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CUSTOMERS:
      return addItemsToState(action.payload.customers, state);
    default:
      return state;
  }
};
