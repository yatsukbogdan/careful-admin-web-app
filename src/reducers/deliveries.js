import { ADD_DELIVERIES, UPDATE_DELIVERY } from '../actions/deliveries';
import { addItemsToState, updateItemInState } from '../utils';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_DELIVERIES:
      return addItemsToState(action.payload.deliveries, state);
    case UPDATE_DELIVERY:
      return updateItemInState(action.payload.delivery, action.payload.id, state);
    default:
      return state;
  }
};
