import {
  ADD_PRODUCT,
  ADD_PRODUCTS,
  UPDATE_PRODUCT,
  REMOVE_PRODUCT,
  TOGGLE_PRODUCT_FEEDBACK_VISIBILITY
} from '../actions/products';
import { addItemsToState, updateItemInState, removeItemFromState, addItemToState } from '../utils';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return addItemToState(action.payload.product, state);
    case UPDATE_PRODUCT:
      return updateItemInState(action.payload.product, action.payload.id, state);
    case REMOVE_PRODUCT:
      return removeItemFromState(action.payload.id, state);
    case ADD_PRODUCTS:
      return addItemsToState(action.payload.products, state);
    case TOGGLE_PRODUCT_FEEDBACK_VISIBILITY: {
      const { productId, feedbackId } = action.payload;
      if (!state[productId]) return;

      const index = state[productId].feedbacks.findIndex(fb => fb.id === feedbackId);
      if (index === -1) return;

      return {
        ...state,
        [productId]: {
          ...state[productId],
          feedbacks: [
            ...state[productId].feedbacks.slice(0, index),
            {
              ...state[productId].feedbacks[index],
              visible: !state[productId].feedbacks[index].visible
            },
            ...state[productId].feedbacks.slice(index + 1)
          ]
        }
      };
    }
    default:
      return state;
  }
};
