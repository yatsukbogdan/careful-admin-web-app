import { ADD_INVOICE, UPDATE_INVOICE, REMOVE_INVOICE, ADD_INVOICES } from '../actions/invoices';
import { addItemToState, updateItemInState, removeItemFromState, addItemsToState } from '../utils';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_INVOICE:
      return addItemToState(action.payload.invoice, state);
    case UPDATE_INVOICE:
      return updateItemInState(action.payload.invoice, action.payload.id, state);
    case REMOVE_INVOICE:
      return removeItemFromState(action.payload.id, state);
    case ADD_INVOICES:
      return addItemsToState(action.payload.invoices, state);
    default:
      return state;
  }
};
