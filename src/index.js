import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import API, { NovaPoshtaAPI } from './api';
import * as serviceWorker from './serviceWorker';

API.init();
NovaPoshtaAPI.init();

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
