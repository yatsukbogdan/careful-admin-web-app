export const ADD_CATEGORIES = 'ADD_CATEGORIES';
export const ADD_CATEGORY = 'ADD_CATEGORY';
export const UPDATE_CATEGORY = 'UPDATE_CATEGORY';
export const REMOVE_CATEGORY = 'REMOVE_CATEGORY';

export const addCategories = payload => ({
  type: ADD_CATEGORIES,
  payload
});

export const addCategory = payload => ({
  type: ADD_CATEGORY,
  payload
});

export const updateCategory = payload => ({
  type: UPDATE_CATEGORY,
  payload
});

export const removeCategory = payload => ({
  type: REMOVE_CATEGORY,
  payload
});
