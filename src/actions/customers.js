export const ADD_CUSTOMERS = 'ADD_CUSTOMERS';

export const addCustomers = payload => ({
  type: ADD_CUSTOMERS,
  payload
});
