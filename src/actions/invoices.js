export const ADD_INVOICES = 'ADD_INVOICES';
export const ADD_INVOICE = 'ADD_INVOICE';
export const UPDATE_INVOICE = 'UPDATE_INVOICE';
export const REMOVE_INVOICE = 'REMOVE_INVOICE';

export const addInvoices = payload => ({
  type: ADD_INVOICES,
  payload
});

export const addInvoice = payload => ({
  type: ADD_INVOICE,
  payload
});

export const updateInvoice = payload => ({
  type: UPDATE_INVOICE,
  payload
});

export const removeInvoice = payload => ({
  type: REMOVE_INVOICE,
  payload
});
