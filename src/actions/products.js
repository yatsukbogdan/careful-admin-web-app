export const ADD_PRODUCTS = 'ADD_PRODUCTS';
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const TOGGLE_PRODUCT_FEEDBACK_VISIBILITY = 'TOGGLE_PRODUCT_FEEDBACK_VISIBILITY';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';

export const addProducts = payload => ({
  type: ADD_PRODUCTS,
  payload
});

export const addProduct = payload => ({
  type: ADD_PRODUCT,
  payload
});

export const updateProduct = payload => ({
  type: UPDATE_PRODUCT,
  payload
});

export const toggleProductFeedbackVisibility = payload => ({
  type: TOGGLE_PRODUCT_FEEDBACK_VISIBILITY,
  payload
});

export const removeProduct = payload => ({
  type: REMOVE_PRODUCT,
  payload
});
