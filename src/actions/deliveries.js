export const ADD_DELIVERIES = 'ADD_DELIVERIES';
export const UPDATE_DELIVERY = 'UPDATE_DELIVERY';

export const addDeliveries = payload => ({
  type: ADD_DELIVERIES,
  payload
});

export const updateDelivery = payload => ({
  type: UPDATE_DELIVERY,
  payload
});
