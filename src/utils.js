import { css } from 'styled-components';
import { EMAIL_REGEX, PHONE_REGEX } from './config';

export const numberToCurrency = (number, currency) => {
  if (number == null) return '';
  return number.toLocaleString('uk-UA', {
    style: 'currency',
    currency,
    minimumFractionDigits: 0
  });
};

export const deliveryOptions = [
  {
    id: 0,
    text: 'Новая почта'
  },
  {
    id: 1,
    text: 'Укрпочта'
  }
];

export const nameValidation = name => {
  if (name.length === 0) return { success: false, error: 'Поле Имя не может быть пустым' };
  if (name.length > 0 && name.length < 3) return { success: false, error: 'Имя не правильное' };
  return { success: true };
};

export const surnameValidation = surname => {
  if (surname.length === 0) return { success: false, error: 'Поле Фамилия не может быть пустым' };
  if (surname.length > 0 && surname.length < 3) return { success: false, error: 'Фамилия не правильная' };
  return { success: true };
};

export const emailValidation = email => {
  if (email.length === 0) return { success: false, error: 'Поле электронная почта не может быть пустым' };
  if (!EMAIL_REGEX.test(email.toLowerCase()))
    return { success: false, error: 'Электронная почта введена не правильно' };
  return { success: true };
};

export const phoneValidation = phone => {
  if (phone.length === 0) return { success: false, error: 'Поле номер телефона не может быть пустым' };
  if (!PHONE_REGEX.test(phone.toLowerCase())) return { success: false, error: 'Номер телефона введен не правильно' };
  return { success: true };
};

export const b64EncodeUnicode = str => {
  return btoa(
    encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
      return String.fromCharCode('0x' + p1);
    })
  );
};

export const binaryArrayToString = array => array.map(code => String.fromCharCode(code)).join('');

const sizes = {
  retina: 3000,
  desktop: 992,
  tablet: 768,
  phone: 576,
  smallPhone: 320
};

export const mapNovaPoshtaCities = city => ({ id: city.Ref, text: city.DescriptionRu });
export const mapNovaPoshtaAdresses = adress => ({
  id: adress.Ref,
  text: adress.DescriptionRu.replace(/[Оо]тделение/, '')
});

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)}
    }
  `;

  return acc;
}, {});

export const normilizeData = data =>
  data.reduce((obj, item) => {
    obj[item.id] = item;
    return obj;
  }, {});

export const addItemToState = (item, state) => ({
  ...state,
  [item.id]: item
});

export const addItemsToState = (items, state) => ({
  ...state,
  ...normilizeData(items)
});

export const updateItemInState = (item, id, state) => ({
  ...state,
  [id]: {
    ...state[id],
    ...item
  }
});

export const removeItemFromState = (id, state) => {
  const { [id]: value, ...items } = state;
  return items;
};
