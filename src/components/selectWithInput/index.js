import styled, { css, keyframes } from 'styled-components';
import React, { Component } from 'react';
import { IoIosArrowDown, IoIosCheckmark } from 'react-icons/io';
import { COLORS } from '../../config';
import { Spinner } from 'react-activity';
import 'react-activity/dist/react-activity.css';

const Container = styled.div`
  transition: 0.2s;
  ${props =>
    props.disabled &&
    css`
      opacity: 0.4;
      pointer-events: none;
    `}
  position: relative;
  margin-bottom: 10px;
`;

const InputContainer = styled.div`
  position: relative;
  box-sizing: border-box;
  height: 40px;
  display: flex;
  align-items: center;
  padding: 0 5px 0 10px;
  margin-bottom: 30px;
  border: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
  ${props =>
    props.focused &&
    css`
      border: 1px ${COLORS.MAIN_COLOR} solid;
    `};
  ${props =>
    props.error &&
    css`
      border: 1px ${COLORS.LIGHT_RED} solid;
      margin-bottom: 0;
    `};
`;

const InputComponent = styled.input`
  font-size: 18px;
  font-weight: 100;
  font-family: 'TT Norms';
  width: 100%;

  :focus {
    outline: none;
  }
`;

const ANIMATION_DURATION_MILLISECONDS = 200;
const shakeAnimation = keyframes`
  20%, 80% {
    transform: translate3d(-2px, 0, 0);
  }
  40%, 60% {
    transform: translate3d(2px, 0, 0);
  }
`;

const Label = styled.p`
  color: ${COLORS.GRAY};
  ${props =>
    props.error &&
    css`
      color: ${COLORS.LIGHT_RED};
    `};
  ${props =>
    props.errorAnimation &&
    css`
      animation: ${shakeAnimation} ${ANIMATION_DURATION_MILLISECONDS}ms;
    `}
  font-weight: 400;
  margin-bottom: 5px;
  font-size: 16px;
`;

const PendingContainer = styled.div`
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: 20;
  background-color: rgba(255, 255, 255, 0.4);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ArrowContainer = styled.div`
  transition: 0.3s;
  transform: ${props => (props.down ? 'rotate(0deg)' : 'rotate(180deg)')};
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  user-select: none;
  cursor: pointer;
`;

const SelectContainer = styled.div`
  overflow-y: ${props => (props.pending ? 'hidden' : 'scroll')};
  z-index: 10;
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  background-color: white;
  top: 63px;
  max-height: 200px;
  border: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
`;

const EmptyListContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
`;

const EmptyListText = styled.p`
  font-size: 16px;
  color: ${COLORS.LIGHT_GRAY};
  letter-spacing: 1px;
`;

const Separator = styled.div`
  height: 1px;
  margin: 0 15px;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

const OptionContainer = styled.div`
  display: flex;
  cursor: pointer;
  user-select: none;
  align-items: center;
  justify-content: space-between;
  padding: 0 15px;
`;

const OptionText = styled.p`
  margin: 10px 0;
  font-size: 16px;
  letter-spacing: 2px;
  font-weight: 100;
`;

const ErrorContainer = styled.div`
  display: flex;
  height: 20px;
  align-items: center;
`;

const ErrorText = styled.p`
  text-transform: uppercase;
  color: ${COLORS.LIGHT_RED};
  font-size: 10px;
`;

export default class SelectWithInput extends Component {
  static defaultProps = {
    onBlur: () => false,
    pending: false
  };

  state = {
    focused: false,
    errorAnimation: false,
    opened: false,
    filterEnabled: false
  };

  onBlurPending = false;

  startErrorAnimation = () => {
    this.setState({ errorAnimation: true });
    setTimeout(() => this.setState({ errorAnimation: false }), ANIMATION_DURATION_MILLISECONDS);
  };

  getFilteredOptions = () => {
    if (
      !this.props.filterByInputEnabled ||
      this.props.value.length === 0 ||
      !this.state.focused ||
      !this.state.filterEnabled
    )
      return this.props.options;
    return this.props.options.filter(
      option => option.text.toLowerCase().indexOf(this.props.value.toLowerCase()) !== -1
    );
  };

  toggleSelect = () => {
    if (this.state.opened) {
      this.close();
      return;
    }

    this.open();
  };

  onBlur = () => {
    if (!this.onBlurPending) return;
    this.props.onBlur();
    this.setState({ opened: false, focused: false });
  };

  open = () => this.setState({ opened: true });
  close = () => this.setState({ opened: false });

  onChange = option => {
    this.close();
    this.props.onChange(option);
    if (!this.onBlurPending) return;
    this.onBlur();
    this.onBlurPending = false;
  };

  renderSelect = () => {
    if (!this.state.opened) return;
    const { choosenOptionId } = this.props;
    const options = this.getFilteredOptions();

    return (
      <SelectContainer pending={this.props.pending}>
        {options.length > 0 ? (
          options.map((option, index) => (
            <div key={option.id}>
              {index !== 0 && <Separator />}
              <OptionContainer onClick={() => this.onChange(option)}>
                <OptionText>{option.text}</OptionText>
                {option.id === choosenOptionId && <IoIosCheckmark size={30} color={COLORS.MAIN_COLOR} />}
              </OptionContainer>
            </div>
          ))
        ) : (
          <EmptyListContainer>
            <EmptyListText>{this.props.emptyListText}</EmptyListText>
          </EmptyListContainer>
        )}
        {this.props.pending && (
          <PendingContainer>
            <Spinner size={20} color={COLORS.MAIN_COLOR} />
          </PendingContainer>
        )}
      </SelectContainer>
    );
  };

  onFocus = () => this.setState({ focused: true, opened: true, filterEnabled: false });

  render() {
    const { label, errorText, errorVisible, value, disabled, style } = this.props;
    const { opened } = this.state;

    return (
      <Container style={style ? style : {}} disabled={disabled}>
        <Label errorAnimation={this.state.errorAnimation} error={errorVisible}>
          {label}
        </Label>
        <InputContainer focused={this.state.focused} error={errorVisible}>
          <InputComponent
            type='text'
            ref={input => (this.input = input)}
            value={value}
            onSubmit={this.props.onSubmit}
            placeholder={this.props.placeholder}
            onChange={evt => {
              this.props.onInputChange(evt.target.value);
              this.setState({ filterEnabled: true });
            }}
            onFocus={this.onFocus}
            onBlur={() => {
              this.onBlurPending = true;
              setTimeout(this.onBlur, 200);
            }}
          />
          <ArrowContainer onClick={this.toggleSelect} down={!opened}>
            <IoIosArrowDown size={20} color={COLORS.MAIN_COLOR} />
          </ArrowContainer>
        </InputContainer>
        {errorVisible && (
          <ErrorContainer>
            <ErrorText>{errorText}</ErrorText>
          </ErrorContainer>
        )}
        {this.renderSelect()}
      </Container>
    );
  }
}
