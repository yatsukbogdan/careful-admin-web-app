import React, { Component } from 'react';
import styled from 'styled-components';
import { MdClear, MdEdit } from 'react-icons/md';
import { COLORS } from '../../config';

const Container = styled.div`
  padding: 10px;
  border: 2px ${COLORS.VERY_LIGHT_GRAY} solid;
  position: relative;
  min-height: 100px;
`;

const ActionButton = styled.div`
  height: 24px;
  width: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

const ButtonsContainer = styled.div`
  position: absolute;
  right: 10px;
  top: 10px;
  z-index: 9;
`;

const EditButton = styled(ActionButton)`
  border: 2px ${COLORS.YELLOW} solid;
  margin-bottom: 10px;
`;

const RemoveButton = styled(ActionButton)`
  border: 2px ${COLORS.LIGHT_RED} solid;
`;

export default class InstanceItemInList extends Component {
  static defaultProps = {
    editButtonVisible: true,
    deleteButtonVisible: true
  };

  renderEditButton = () => {
    if (!this.props.editButtonVisible) return false;
    return (
      <EditButton onClick={this.props.onEditClick}>
        <MdEdit size={15} color={COLORS.YELLOW} />
      </EditButton>
    );
  };

  renderDeleteButton = () => {
    if (!this.props.deleteButtonVisible) return false;
    return (
      <RemoveButton onClick={this.props.onDeleteClick}>
        <MdClear size={30} color={COLORS.LIGHT_RED} />
      </RemoveButton>
    );
  };

  render() {
    return (
      <Container>
        <ButtonsContainer>
          {this.renderEditButton()}
          {this.renderDeleteButton()}
        </ButtonsContainer>
        {this.props.children}
      </Container>
    );
  }
}
