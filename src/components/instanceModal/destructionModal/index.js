import React, { Component } from 'react';
import { OuterContainer, SmallInnerContainer } from '../styles';
import styled from 'styled-components';
import Button from '../../button';
import { COLORS } from '../../../config';

const DestructionModalOuterContainer = styled(OuterContainer)`
  z-index: ${props => (props.visible ? 300 : 201)};
`;

const ButtonContainer = styled.div`
  display: flex;
`;

const Text = styled.p`
  font-size: 20px;
  margin-bottom: 20px;
`;

class DestructionModal extends Component {
  onOuterContainerClick = ev => {
    this.props.close();
    ev.stopPropagation();
  };

  onDeleteClick = () => {
    this.props.close();
    this.props.onDeleteClick();
  };

  render() {
    return (
      <DestructionModalOuterContainer onClick={this.onOuterContainerClick} visible={this.props.visible}>
        <SmallInnerContainer onClick={ev => ev.stopPropagation()} visible={this.props.visible}>
          <Text>{this.props.message}</Text>
          <ButtonContainer>
            <Button
              label={this.props.destructionButtonText}
              onClick={this.onDeleteClick}
              style={{ flex: 1, marginRight: '15px', borderColor: COLORS.LIGHT_RED }}
              textStyle={{ letterSpacing: 1, fontSize: '14px' }}
            />
            <Button
              label='Отмена'
              onClick={this.props.close}
              style={{ flex: 1, borderColor: COLORS.VERY_LIGHT_GRAY }}
              textStyle={{ letterSpacing: 1, fontSize: '14px' }}
            />
          </ButtonContainer>
        </SmallInnerContainer>
      </DestructionModalOuterContainer>
    );
  }
}

export default DestructionModal;
