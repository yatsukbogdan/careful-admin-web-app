import React, { Component } from 'react';
import { OuterContainer, BigInnerContainer } from './styles';
import styled from 'styled-components';
import Button from '../button';
import { COLORS } from '../../config';

const ButtonsContainer = styled.div`
  display: flex;
  padding-top: 20px;
  border-top: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
`;

const Label = styled.p`
  font-size: 18px;
  padding-bottom: 20px;
  border-bottom: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
`;

export default class InstanceModal extends Component {
  render() {
    return (
      <OuterContainer onClick={this.props.close} visible={this.props.visible}>
        <BigInnerContainer onClick={ev => ev.stopPropagation()} visible={this.props.visible}>
          <Label>{this.props.label}</Label>
          {this.props.children}
          <ButtonsContainer>
            <Button
              style={{ flex: 1, marginRight: '15px' }}
              onClick={this.props.onActionButtonClick}
              label={this.props.action === 'edit' ? 'Обновить' : 'Добавить'}
            />
            {this.props.action === 'edit' && (
              <Button
                style={{ flex: 1, marginRight: '15px', borderColor: COLORS.LIGHT_RED }}
                onClick={this.props.onRemoveButtonClick}
                label='Удалить'
              />
            )}
            {
              <Button
                style={{ flex: 1, borderColor: COLORS.VERY_LIGHT_GRAY }}
                onClick={this.props.onCancelButtonClick}
                label='Отменить'
              />
            }
          </ButtonsContainer>
        </BigInnerContainer>
      </OuterContainer>
    );
  }
}
