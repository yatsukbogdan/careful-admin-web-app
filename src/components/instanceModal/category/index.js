import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import Input from '../../input';
import { nameValidation } from './validations';
import API from '../../../api';
import { addCategory, updateCategory, removeCategory } from '../../../actions/categories';
import InstanceModal from '..';
import { FormContainer } from '../styles';

class CategoryModal extends Component {
  state = {
    name: '',
    nameError: false,
    nameErrorText: null
  };

  clearAllFields = () => {
    this.setState({
      name: '',
      nameError: false,
      nameErrorText: null
    });
  };

  validateField = async field => {
    let validationFunction;

    switch (field) {
      case 'name':
        validationFunction = nameValidation;
        break;
      default:
        validationFunction = nameValidation;
        break;
    }

    const validationObject = validationFunction(this.state[field]);

    if (validationObject.success) return;

    await this.setState({
      [`${field}Error`]: true,
      [`${field}ErrorText`]: validationObject.error
    });
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.visible && !nextProps.visible) {
      this.clearAllFields();
    }
    if (!this.props.visible && nextProps.visible && nextProps.action === 'edit') {
      this.setState({ name: nextProps.category.name });
    }
  }
  validateAllFields = async () => {
    await Promise.all([this.validateField('name')]);
  };

  onFieldChange = (value, field) =>
    this.setState({
      [field]: value,
      [`${field}Error`]: false,
      [`${field}ErrorText`]: null
    });

  onCancelButtonClick = () => {
    this.clearAllFields();
    this.props.close();
  };

  onActionButtonClick = async () => {
    await this.validateAllFields();
    const { nameError } = this.state;

    if (nameError) {
      if (nameError) this.nameInput.startErrorAnimation();
      return;
    }

    const { name } = this.state;
    let data;
    if (this.props.action === 'edit') {
      data = (await API.updateCategory(this.props.categoryId, name)).data;
    } else {
      data = (await API.addCategory({ name })).data;
    }
    if (!data.success) {
      alert(data.message);
      return;
    }
    if (this.props.action === 'edit') {
      this.props.updateCategory({ id: this.props.categoryId, category: data.payload.category });
    } else {
      this.props.addCategory({ category: data.payload.category });
    }
    this.clearAllFields();
    this.props.close();
  };
  render() {
    return (
      <InstanceModal
        label={this.props.action === 'edit' && this.props.category ? this.props.category.name : 'Новая категория'}
        visible={this.props.visible}
        close={this.props.close}
        action={this.props.action}
        onActionButtonClick={this.onActionButtonClick}
        onCancelButtonClick={this.onCancelButtonClick}
        onRemoveButtonClick={this.props.onRemoveButtonClick}
      >
        <FormContainer>
          <Input
            value={this.state.name}
            label='Название'
            onChange={value => this.onFieldChange(value, 'name')}
            onBlur={() => this.validateField('name')}
            ref={input => (this.nameInput = input)}
            errorVisible={this.state.nameError}
            errorText={this.state.nameErrorText}
          />
        </FormContainer>
      </InstanceModal>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const props = {};
  if (ownProps.action === 'edit' && ownProps.categoryId) {
    props.category = state.categories[ownProps.categoryId];
  }
  return props;
};

const mapDispatchToProps = {
  addCategory,
  updateCategory,
  removeCategory
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CategoryModal)
);
