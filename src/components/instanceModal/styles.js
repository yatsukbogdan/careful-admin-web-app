import styled from 'styled-components';

export const OuterContainer = styled.div`
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  z-index: ${props => (props.visible ? 200 : -1)};
  position: fixed;
  height: 100vh;
  width: 100vw;
  align-items: center;
  justify-content: center;
  display: flex;
  transition: 0.4s;
  top: 0;
  background-color: ${props => (props.visible ? 'rgba(0, 0, 0, 0.5)' : 'transparent')};
`;

const InnerContainer = styled.div`
  box-sizing: border-box;
  padding: 20px;
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: white;
  transition: 0.4s;
  transform: ${props => (props.visible ? 'translate(0, 0)' : 'translate(0, 100vh)')};
`;

export const BigInnerContainer = styled(InnerContainer)`
  height: 70%;
  width: 90%;
`;

export const SmallInnerContainer = styled(InnerContainer)`
  width: 600px;
`;

export const FormContainer = styled.div`
  flex: 1;
  padding: 20px 0;
  overflow-y: scroll;
`;
