import React, { Component } from 'react';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import { connect } from 'react-redux';
import uuid from 'uuid';
import EXIF from 'exif-js';
import { addProduct, updateProduct, removeProduct } from '../../../actions/products';
import API from '../../../api';
import Button from '../../button';
import Input from '../../input';
import Select from '../../select';
import {
  nameValidation,
  descriptionValidation,
  codeValidation,
  priceValidation,
  discountPriceValidation
} from './validations';
import { objectIdValidation } from '../validations';
import { COLORS } from '../../../config';
import DragAndDropImages from './components/dragAndDropImages';
import InstanceModal from '..';
import { FormContainer } from '../styles';

const NoImageContainer = styled.div`
  margin-bottom: 15px;
  user-select: none;
  cursor: pointer;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
  height: 100px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const NoImageText = styled.p`
  font-size: 20px;
  color: ${COLORS.GRAY};
`;

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const readURL = (file, callback) => {
  const reader = new FileReader();

  reader.onload = e => {
    if (typeof callback === 'function') callback(e.target.result);
  };
  reader.readAsDataURL(file);
};

class ProductModal extends Component {
  state = {
    code: '',
    codeError: false,
    codeErrorText: null,

    name: '',
    nameError: false,
    nameErrorText: null,

    siteName: '',
    siteNameError: false,
    siteNameErrorText: null,

    purchasePrice: '',
    purchasePriceError: false,
    purchasePriceErrorText: null,

    retailPrice: '',
    retailPriceError: false,
    retailPriceErrorText: null,

    categoryId: null,
    categoryIdError: false,
    categoryIdErrorText: null,

    description: '',
    descriptionError: false,
    descriptionErrorText: null,

    discountPrice: '',
    discountPriceError: false,
    discountPriceErrorText: null,

    album: []
  };

  onAddClick = () => false;

  validateField = async field => {
    let validationFunction;

    switch (field) {
      case 'code':
        validationFunction = codeValidation;
        break;
      case 'name':
      case 'siteName':
        validationFunction = nameValidation;
        break;
      case 'purchasePrice':
      case 'retailPrice':
        validationFunction = priceValidation;
        break;
      case 'categoryId':
        validationFunction = objectIdValidation;
        break;
      case 'description':
        validationFunction = descriptionValidation;
        break;
      case 'discountPrice':
        validationFunction = discountPriceValidation;
        break;
      default:
        validationFunction = nameValidation;
        break;
    }

    const validationObject = validationFunction(this.state[field]);

    if (validationObject.success) return;

    await this.setState({
      [`${field}Error`]: true,
      [`${field}ErrorText`]: validationObject.error
    });
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.visible && !nextProps.visible) {
      this.clearAllFields();
    }
    if (!this.props.visible && nextProps.visible && nextProps.action === 'edit') {
      this.setState({
        code: nextProps.product.code,
        name: nextProps.product.name,
        siteName: nextProps.product.siteName,
        purchasePrice: nextProps.product.purchasePrice.toString(),
        retailPrice: nextProps.product.retailPrice.toString(),
        categoryId: nextProps.product.categoryId,
        description: nextProps.product.description,
        discountPrice: nextProps.product.discountPrice ? nextProps.product.discountPrice.toString() : '',
        album: nextProps.product.album
      });
    }
  }

  validateAllFields = async () => {
    await Promise.all([
      this.validateField('code'),
      this.validateField('name'),
      this.validateField('siteName'),
      this.validateField('purchasePrice'),
      this.validateField('retailPrice'),
      this.validateField('categoryId'),
      this.validateField('description'),
      this.validateField('discountPrice')
    ]);
  };

  clearAllFields = () => {
    this.cleerField('code');
    this.cleerField('name');
    this.cleerField('siteName');
    this.cleerField('purchasePrice');
    this.cleerField('retailPrice');
    this.cleerField('categoryId');
    this.cleerField('description');
    this.cleerField('discountPrice');
    this.setState({ album: [] });
  };

  cleerField = field => {
    if (field === 'categoryId') {
      this.setState({
        [field]: null,
        [`${field}Error`]: false,
        [`${field}ErrorText`]: null
      });
      return;
    }
    this.setState({
      [field]: '',
      [`${field}Error`]: false,
      [`${field}ErrorText`]: null
    });
  };

  onFieldChange = (value, field) =>
    this.setState({
      [field]: value,
      [`${field}Error`]: false,
      [`${field}ErrorText`]: null
    });

  onCancelButtonClick = () => {
    this.clearAllFields();
    this.props.close();
  };

  reorderImages = result => {
    const album = reorder(this.state.album, result.source.index, result.destination.index);
    this.setState({ album });
  };

  onActionButtonClick = async () => {
    await this.validateAllFields();

    const {
      nameError,
      siteNameError,
      codeError,
      purchasePriceError,
      retailPriceError,
      categoryIdError,
      descriptionError,
      discountPriceError
    } = this.state;

    if (
      nameError ||
      siteNameError ||
      codeError ||
      purchasePriceError ||
      retailPriceError ||
      categoryIdError ||
      descriptionError ||
      discountPriceError
    ) {
      if (nameError) this.nameInput.startErrorAnimation();
      if (siteNameError) this.siteNameInput.startErrorAnimation();
      if (codeError) this.codeInput.startErrorAnimation();
      if (purchasePriceError) this.purchasePriceInput.startErrorAnimation();
      if (retailPriceError) this.retailPriceInput.startErrorAnimation();
      if (categoryIdError) this.categoryIdInput.startErrorAnimation();
      if (descriptionError) this.descriptionInput.startErrorAnimation();
      if (discountPriceError) this.discountPriceInput.startErrorAnimation();
      return;
    }

    const album = [...this.state.album];
    const product = {
      name: this.state.name,
      siteName: this.state.siteName,
      code: this.state.code,
      purchasePrice: this.state.purchasePrice,
      retailPrice: this.state.retailPrice,
      categoryId: this.state.categoryId,
      description: this.state.description,
      discountPrice: this.state.discountPrice
    };
    let data;
    if (this.props.action === 'edit') {
      data = (await API.updateProduct(this.props.productId, product)).data;
    } else {
      data = (await API.addProduct(product)).data;
    }

    if (!data.success) {
      alert(data.message);
      return;
    }

    if (this.props.action === 'edit') {
      this.props.updateProduct({ id: this.props.productId, product: data.payload.product });
    } else {
      this.props.addProduct({ product: data.payload.product });
    }

    for (const albumObj of album) {
      if (!albumObj.file) continue;
      const body = new FormData();
      body.append('image', albumObj.file, albumObj.file.name);
      body.append('imageId', albumObj.id);
      if (typeof albumObj.orientation !== 'undefined') {
        body.append('orientation', albumObj.orientation);
      }

      const imgResponseData = (await API.uploadProductImage(body, data.payload.product.id)).data;
      if (!imgResponseData.success) {
        alert(imgResponseData.message);
        return;
      }
    }
    album.forEach(async albumObj => {});
    const order = album.map(obj => obj.id);
    const reorderResponseData = (await API.updateProductAlbumOrder({ order }, data.payload.product.id)).data;
    if (!reorderResponseData.success) {
      alert(reorderResponseData.message);
      return;
    }

    this.props.updateProduct({ id: data.payload.product.id, product: { album: reorderResponseData.payload.album } });

    this.clearAllFields();
    this.props.close();
  };

  removeImage = id => {
    const index = this.state.album.findIndex(img => img.id === id);
    if (index === -1) return;

    this.setState({
      album: [...this.state.album.slice(0, index), ...this.state.album.slice(index + 1)]
    });
  };

  addImage = (url, file, orientation) => {
    this.setState({
      album: [
        ...this.state.album,
        {
          url,
          file,
          orientation,
          id: uuid()
        }
      ]
    });
  };

  onFileChange = () => {
    if (this.fileInput && this.fileInput.files && this.fileInput.files[0]) {
      const file = this.fileInput.files[0];
      EXIF.getData(file, c => {
        const orientation = EXIF.getTag(file, 'Orientation');
        readURL(file, url => this.addImage(url, file, orientation));
      });
    }
  };

  render() {
    return (
      <InstanceModal
        label={this.props.action === 'edit' && this.props.product ? this.props.product.name : 'Новый товар'}
        visible={this.props.visible}
        close={this.props.close}
        action={this.props.action}
        onActionButtonClick={this.onActionButtonClick}
        onCancelButtonClick={this.onCancelButtonClick}
        onRemoveButtonClick={this.props.onRemoveButtonClick}
      >
        <FormContainer>
          <Input
            value={this.state.code}
            label='Код товара'
            onChange={value => this.onFieldChange(value, 'code')}
            onBlur={() => this.validateField('code')}
            ref={input => (this.codeInput = input)}
            errorVisible={this.state.codeError}
            errorText={this.state.codeErrorText}
          />
          <Input
            value={this.state.name}
            label='Наименование'
            onChange={value => this.onFieldChange(value, 'name')}
            onBlur={() => this.validateField('name')}
            ref={input => (this.nameInput = input)}
            errorVisible={this.state.nameError}
            errorText={this.state.nameErrorText}
          />
          <Input
            value={this.state.siteName}
            label='Название на сайте'
            onChange={value => this.onFieldChange(value, 'siteName')}
            onBlur={() => this.validateField('siteName')}
            ref={input => (this.siteNameInput = input)}
            errorVisible={this.state.siteNameError}
            errorText={this.state.siteNameErrorText}
          />
          <Input
            value={this.state.purchasePrice}
            label='Закупочная цена'
            onChange={value => this.onFieldChange(value, 'purchasePrice')}
            onBlur={() => this.validateField('purchasePrice')}
            ref={input => (this.purchasePriceInput = input)}
            errorVisible={this.state.purchasePriceError}
            errorText={this.state.purchasePriceErrorText}
          />
          <Input
            value={this.state.retailPrice}
            label='Розничная цена'
            onChange={value => this.onFieldChange(value, 'retailPrice')}
            onBlur={() => this.validateField('retailPrice')}
            ref={input => (this.retailPriceInput = input)}
            errorVisible={this.state.retailPriceError}
            errorText={this.state.retailPriceErrorText}
          />
          <Select
            onChange={value => this.onFieldChange(value, 'categoryId')}
            label='Категория'
            ref={input => (this.categoryIdInput = input)}
            options={this.props.categories}
            choosenOptionId={this.state.categoryId}
            placeholder='Выберите категорию товаров'
            errorVisible={this.state.categoryIdError}
            errorText={this.state.categoryIdErrorText}
          />
          {this.state.album.length === 0 && (
            <NoImageContainer onClick={() => this.fileInput.click()}>
              <NoImageText>Добавить изображение</NoImageText>
            </NoImageContainer>
          )}
          <DragAndDropImages
            images={this.state.album}
            removeImage={this.removeImage}
            reorderImages={this.reorderImages}
          />
          {this.state.album.length !== 0 && (
            <Button onClick={() => this.fileInput.click()} label='Добавить изображение' />
          )}
          <input hidden={true} ref={i => (this.fileInput = i)} name='myFile' type='file' onChange={this.onFileChange} />
          <Input
            value={this.state.description}
            label='Описание'
            multiline={true}
            onChange={value => this.onFieldChange(value, 'description')}
            onBlur={() => this.validateField('description')}
            ref={input => (this.descriptionInput = input)}
            errorVisible={this.state.descriptionError}
            errorText={this.state.descriptionErrorText}
          />
          <Input
            value={this.state.discountPrice}
            label='Цена со скидкой'
            onChange={value => this.onFieldChange(value, 'discountPrice')}
            onBlur={() => this.validateField('discountPrice')}
            ref={input => (this.discountPriceInput = input)}
            errorVisible={this.state.discountPriceError}
            errorText={this.state.discountPriceErrorText}
          />
        </FormContainer>
      </InstanceModal>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const props = {
    categories: Object.keys(state.categories)
      .map(id => state.categories[id])
      .map(cat => ({
        id: cat.id,
        text: cat.name
      }))
  };

  if (ownProps.action === 'edit' && ownProps.productId) {
    props.product = state.products[ownProps.productId];
  }
  return props;
};

const mapDispatchToProps = {
  addProduct,
  updateProduct,
  removeProduct
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProductModal)
);
