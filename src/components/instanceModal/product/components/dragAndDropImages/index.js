import React, { Component } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import styled from 'styled-components';
import DraggableImage from './draggableImage';
import { COLORS } from '../../../../../config';

const Container = styled.div`
  margin-bottom: 15px;
  display: flex;
  padding: 8px;
  overflow: auto;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

export default class DragAndDropImages extends Component {
  onDragEnd = result => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    this.props.reorderImages(result);
  };

  render() {
    const { images } = this.props;
    if (images.length === 0) return false;

    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId='droppable' direction='horizontal'>
          {(provided, snapshot) => (
            <Container ref={provided.innerRef} {...provided.droppableProps}>
              {images.map((img, index) => (
                <DraggableImage
                  onRemoveClick={() => this.props.removeImage(img.id)}
                  key={index}
                  image={img}
                  index={index}
                />
              ))}
              {provided.placeholder}
            </Container>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}
