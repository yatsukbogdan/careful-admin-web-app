import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { MdClear } from 'react-icons/md';
import { Draggable } from 'react-beautiful-dnd';
import { COLORS } from '../../../../../../config';
import styled, { css } from 'styled-components';

const ImageContainer = styled.div`
  position: relative;
`;

const Image = styled.img`
  width: 120px;
  height: 120px;
  ${props => {
    switch (props.orientation) {
      case 2:
        return css`
          transform: scaleX(-1);
        `;
      case 3:
        return css`
          transform: rotate(180deg);
        `;
      case 4:
        return css`
          transform: scaleX(-1) rotate(180deg);
        `;
      case 5:
        return css`
          transform: scaleX(-1) rotate(-90deg);
        `;
      case 6:
        return css`
          transform: rotate(90deg);
        `;
      case 7:
        return css`
          transform: scaleX(-1) rotate(90deg);
        `;
      case 8:
        return css`
          transform: rotate(-90deg);
        `;
      case 1:
      default:
        return null;
    }
  }}
`;

const RemoveButton = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  height: 24px;
  width: 24px;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  background-color: ${COLORS.LIGHT_RED};
`;

const getItemStyle = (isDragging, draggableStyle) => {
  return {
    // some basic styles to make the items look a bit nicer
    padding: '8px',
    userSelect: 'none',
    margin: '0 8px 0 0',

    // styles we need to apply on draggables
    ...draggableStyle
  };
};

export default class DraggableImage extends Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
    document.body.appendChild(this.el);
  }

  renderElement = (item, provided, snapshot) => (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
    >
      <ImageContainer>
        <RemoveButton onClick={this.props.onRemoveClick}>
          <MdClear size={15} color='white' />
        </RemoveButton>
        <Image orientation={item.orientation} src={item.url} />
      </ImageContainer>
    </div>
  );

  render() {
    const { index, image } = this.props;
    return (
      <Draggable draggableId={image.id} index={index}>
        {(provided, snapshot) =>
          snapshot.isDragging
            ? ReactDOM.createPortal(this.renderElement(image, provided, snapshot), this.el)
            : this.renderElement(image, provided, snapshot)
        }
      </Draggable>
    );
  }
}
