export const nameValidation = name => {
  if (name.length === 0) return { success: false, error: 'Поле Наменование товара не может быть пустым' };
  if (name.length > 0 && name.length < 3) return { success: false, error: 'Наменование товара не правильное' };
  return { success: true };
};

export const priceValidation = price => {
  if (price.length === 0) return { success: false, error: 'Цена не указана' };
  if (isNaN(price)) return { success: false, error: 'Цена не правильная' };
  return { success: true };
};

export const discountPriceValidation = price => {
  if (!price) return { success: true };
  if (isNaN(price)) return { success: false, error: 'Цена не правильная' };
  return { success: true };
};

export const codeValidation = code => {
  if (!/^[0-9]{6}$/.test(code)) return { success: false, error: 'Код должен состоят из 6 цифр' };
  return { success: true };
};

export const descriptionValidation = desc => {
  if (desc.length === 0) return { success: false, error: 'Поле Описание не может быть пустым' };
  return { success: true };
};
