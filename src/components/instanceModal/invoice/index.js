import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { MdClear } from 'react-icons/md';
import Input from '../../input';
import SelectWithInput from '../../selectWithInput';
import API from '../../../api';
import { numberToCurrency } from '../../../utils';
import { addInvoice, updateInvoice } from '../../../actions/invoices';
import InstanceModal from '..';
import Button from '../../button';
import { FormContainer } from '../styles';
import { COLORS } from '../../../config';

const ProductItemContainer = styled.div`
  display: flex;
`;

const Label = styled.p`
  border-top: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
  padding: 10px 0;
  text-align: right;
  font-size: 20px;
`;

const RemoveButton = styled.div`
  align-self: flex-end;
  box-sizing: border-box;
  margin-bottom: 40px;
  height: 40px;
  width: 40px;
  cursor: pointer;
  user-select: none;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border: 2px ${COLORS.LIGHT_RED} solid;
`;

const LabelSpan = styled.span`
  font-size: 20px;
  font-weight: bold;
`;

const productSchema = {
  productInput: '',

  id: null,
  idError: false,
  idErrorText: null,

  quantity: '',
  quantityError: false,
  quantityErrorText: null,

  price: '',
  priceError: false,
  priceErrorText: null
};

class InvoiceModal extends Component {
  state = {
    products: []
  };

  clearAllFields = () => this.setState({ products: [] });

  addNewProduct = () => {
    this.setState({ products: this.state.products.concat(productSchema) });
  };

  removeProduct = index =>
    this.setState({
      products: [...this.state.products.slice(0, index), ...this.state.products.slice(index + 1)]
    });

  validateAllFields = async () => {
    const products = [...this.state.products];
    for (const index in products) {
      await this.validateProductId(Number(index));
      await this.validateProductPrice(Number(index));
      await this.validateProductQuantity(Number(index));
    }
  };

  validateProductId = async index => {
    const { id } = this.state.products[index];

    if (id !== null) return;

    await this.setProductFieldError(index, 'id', 'Товар не выбран');
  };

  validateProductQuantity = async index => {
    const { quantity } = this.state.products[index];

    if (quantity === '') await this.setProductFieldError(index, 'quantity', 'Количество товара не указано');
    if (isNaN(quantity)) await this.setProductFieldError(index, 'quantity', 'Количество товара указано не верно');
  };

  validateProductPrice = async index => {
    const { price } = this.state.products[index];

    if (price === '') return;
    if (isNaN(price)) await this.setProductFieldError(index, 'price', 'Цена товара указана не верно');
  };

  modifyProductWithData = async (index, data) => {
    await this.setState({
      products: [
        ...this.state.products.slice(0, index),
        {
          ...this.state.products[index],
          ...data
        },
        ...this.state.products.slice(index + 1)
      ]
    });
  };

  setProductFieldError = async (index, field, errorText) => {
    await this.modifyProductWithData(index, {
      [`${field}Error`]: true,
      [`${field}ErrorText`]: errorText
    });
  };

  onProductInputChange = async (index, value) => {
    await this.modifyProductWithData(index, { productInput: value });
  };

  setProductFieldValue = async (index, field, value) => {
    await this.modifyProductWithData(index, {
      [field]: value,
      [`${field}Error`]: false,
      [`${field}ErrorText`]: null
    });
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.visible && !nextProps.visible) {
      this.clearAllFields();
    }
  }

  onSelectBlur = index => {
    const product = this.state.products[index];
    if (product && product.id && product.productInput !== this.props.stockProductsObject[product.id].name) {
      this.onProductInputChange(index, this.props.stockProductsObject[product.id].name);
    }
  };

  onCancelButtonClick = () => {
    this.clearAllFields();
    this.props.close();
  };

  get sum() {
    return this.state.products.reduce((sum, prod) => {
      let price = 0;
      if (prod.price !== '' && !isNaN(prod.price)) {
        price = Number(prod.price);
      } else if (prod.id !== null && this.props.stockProductsObject[prod.id] != null) {
        price = this.props.stockProductsObject[prod.id].purchasePrice;
      }
      if (prod.quantity !== '' && !isNaN(prod.quantity)) {
        return sum + price * Number(prod.quantity);
      }
      return sum;
    }, 0);
  }

  getComponentsArrayWithErrors = () =>
    this.state.products.reduce((errs, prod, index) => {
      if (prod.idError) errs.push(`productSelect${index}`);
      if (prod.quantityError) errs.push(`quantityInput${index}`);
      if (prod.priceError) errs.push(`priceInput${index}`);
      return errs;
    }, []);

  onActionButtonClick = async () => {
    if (this.state.products.length === 0) {
      alert('Нужно добавить хотябы один товар в накладную');
      return;
    }
    await this.validateAllFields();
    const components = this.getComponentsArrayWithErrors();
    if (components.length !== 0) {
      components.forEach(cmp => this[cmp].startErrorAnimation());
      return;
    }

    const products = this.state.products.map(prod => ({
      id: prod.id,
      quantity: Number(prod.quantity),
      price: prod.price === '' ? null : Number(prod.price)
    }));

    let { data } = await API.addPurchaseInvoice(products);
    if (!data.success) {
      alert(data.message);
      return;
    }
    this.props.addInvoice({ invoice: data.payload.invoice });
    this.clearAllFields();
    this.props.close();
  };

  getOptions = index => {
    const productsIdsToFilterBy = [
      ...this.state.products.slice(0, index).map(prod => prod.id),
      ...this.state.products.slice(index + 1).map(prod => prod.id)
    ];
    return this.props.stockProducts.filter(prod => !productsIdsToFilterBy.includes(prod.id));
  };

  render() {
    return (
      <InstanceModal
        label='Новая приходная накладная'
        visible={this.props.visible}
        close={this.props.close}
        action={this.props.action}
        onActionButtonClick={this.onActionButtonClick}
        onCancelButtonClick={this.onCancelButtonClick}
        onRemoveButtonClick={this.props.onRemoveButtonClick}
      >
        <FormContainer>
          {this.state.products.map((prod, index) => (
            <ProductItemContainer>
              <SelectWithInput
                ref={component => (this[`productSelect${index}`] = component)}
                onChange={async product => {
                  await this.setProductFieldValue(index, 'id', product.id);
                  await this.onProductInputChange(index, product.text);
                }}
                onInputChange={value => this.onProductInputChange(index, value)}
                value={prod.productInput}
                label='Товар'
                filterByInputEnabled={true}
                errorVisible={prod.idError}
                errorText={prod.idErrorText}
                options={this.getOptions(index)}
                choosenOptionId={prod.id}
                style={{ flex: 1, marginRight: '10px' }}
                onBlur={() => this.onSelectBlur(index)}
                placeholder='Выберите товар'
                emptyListText='Пусто'
              />
              <Input
                style={{ width: '120px', marginRight: '10px' }}
                value={prod.quantity}
                label='Количество'
                onChange={value => this.setProductFieldValue(index, 'quantity', value)}
                onBlur={() => this.validateProductQuantity(index)}
                ref={input => (this[`quantityInput${index}`] = input)}
                errorVisible={prod.quantityError}
                errorText={prod.quantityErrorText}
              />
              <Input
                style={{ width: '120px', marginRight: '10px' }}
                value={prod.price}
                label='Цена'
                onChange={value => this.setProductFieldValue(index, 'price', value)}
                onBlur={() => this.validateProductPrice(index)}
                ref={input => (this[`priceInput${index}`] = input)}
                errorVisible={prod.priceError}
                errorText={prod.priceErrorText}
                placeholder={prod.id ? this.props.stockProductsObject[prod.id].purchasePrice : null}
              />
              <RemoveButton onClick={() => this.removeProduct(index)}>
                <MdClear size={25} color={COLORS.LIGHT_RED} />
              </RemoveButton>
            </ProductItemContainer>
          ))}
          <Button onClick={this.addNewProduct} label='Добавить товар' />
        </FormContainer>
        <Label>
          Сума: <LabelSpan>{numberToCurrency(this.sum, 'UAH')}</LabelSpan>
        </Label>
      </InstanceModal>
    );
  }
}

const mapStateToProps = state => ({
  stockProductsObject: state.products,
  stockProducts: Object.keys(state.products).map(id => ({
    text: state.products[id].name,
    id
  }))
});

const mapDispatchToProps = {
  addInvoice,
  updateInvoice
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(InvoiceModal)
);
