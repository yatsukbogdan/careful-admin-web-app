export const nameValidation = name => {
  if (name.length === 0) return { success: false, error: 'Поле Название не может быть пустым' };
  if (name.length > 0 && name.length < 3) return { success: false, error: 'Название категории не правильное' };
  return { success: true };
};
