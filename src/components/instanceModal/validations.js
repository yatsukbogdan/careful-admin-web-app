export const objectIdValidation = id => {
  if (id === null) return { success: false, error: 'ObjectID не указан' };
  if (!/^[a-f\d]{24}$/i.test(id)) return { success: false, error: 'ObjectID не правильный' };
  return { success: true };
};
