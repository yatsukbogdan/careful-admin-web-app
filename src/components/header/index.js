import React, { Component } from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router';
import { IoIosSearch, IoIosMenu } from 'react-icons/io';
import { COLORS } from '../../config';

const Container = styled.div`
  position: fixed;
  width: 100%;
  box-sizing: border-box;
  top: 0;
  z-index: 10;
  height: 60px;
  background-color: white;
  display: flex;
  padding: 0 15px;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
`;

const LeftButtonContainer = styled.div`
  cursor: pointer;
  user-select: none;
  display: flex;
  flex: 1;
`;

const TextContainer = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
`;

const Text = styled.p`
  font-family: 'Source Serif Pro';
  letter-spacing: 5px;
  font-size: 25px;
`;

const RightButtonContainer = styled.div`
  opacity: 0;
  display: flex;
  flex: 1;
  justify-content: flex-end;
`;

class Header extends Component {
  state = {
    searchInputValue: '',
    searchInputVisible: false
  };

  showSearchInput = () => this.setState({ searchInputVisible: true });
  hideSearchInput = () => this.setState({ searchInputVisible: false });

  handleSearchInputChange = event => this.setState({ searchInputValue: event.target.value });
  toMainPage = () => this.props.history.push('/');

  render() {
    return (
      <Container>
        <LeftButtonContainer onClick={this.props.onMenuClick}>
          <IoIosMenu size={20} />
        </LeftButtonContainer>
        <TextContainer onClick={this.toMainPage}>
          <Text>Careful Admin</Text>
        </TextContainer>
        <RightButtonContainer>
          <IoIosSearch onClick={this.props.onLoopClick} size={20} />
        </RightButtonContainer>
      </Container>
    );
  }
}

export default withRouter(Header);
