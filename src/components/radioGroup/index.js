import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  margin-bottom: 15px;
`;

const Label = styled.p`
  margin-bottom: 5px;
  font-weight: bold;
`;

const Option = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 5px;
`;

export default class RadioGroup extends Component {
  render() {
    return (
      <Container>
        <Label>{this.props.label}</Label>
        {this.props.options.map(option => (
          <Option key={option.value}>
            <input
              onChange={evt => this.props.onChange(Number(evt.target.value))}
              value={option.value}
              type='radio'
              name={this.props.groupName}
              checked={this.props.checked === option.value}
            />
            <p>{option.text}</p>
          </Option>
        ))}
      </Container>
    );
  }
}
