import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { Spinner } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { COLORS } from '../../config';

const Container = styled.div`
  box-sizing: border-box;
  position: relative;
  ${props =>
    props.disabled &&
    css`
      filter: grayscale(100%);
      opacity: 0.2;
    `};
  height: 45px;
  border: 2px ${COLORS.MAIN_COLOR} solid;
  align-items: center;
  justify-content: center;
  display: flex;
  cursor: pointer;
  user-select: none;
`;

const Text = styled.p`
  letter-spacing: 6px;
  text-transform: uppercase;
  font-size: 16px;
`;

export default class Button extends Component {
  static defaultProps = {
    pending: false,
    disabled: false
  };

  onClick = () => {
    if (this.props.disabled) return;
    this.props.onClick();
  };

  render() {
    const { label, style, textStyle, disabled } = this.props;

    return (
      <Container
        disabled={disabled}
        onClick={this.onClick}
        style={style ? style : {}}
      >
        {this.props.pending ? (
          <Spinner size={12} color={COLORS.MAIN_COLOR} />
        ) : (
          <Text style={textStyle ? textStyle : {}}>{label}</Text>
        )}
      </Container>
    );
  }
}
