import styled, { css, keyframes } from 'styled-components';
import React, { Component } from 'react';
import { COLORS } from '../../config';

const Container = styled.div`
  ${props =>
    props.disabled &&
    css`
      pointer-events: none;
      opacity: 0.4;
    `}
  margin-bottom: 10px;
`;

const InputContainer = styled.div`
  ${props =>
    !props.multiline &&
    css`
      align-items: center;
      height: 40px;
    `}
  box-sizing: border-box;
  display: flex;
  padding: 0 10px;
  margin-bottom: 30px;
  border: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
  ${props =>
    props.focused &&
    css`
      border: 1px ${COLORS.MAIN_COLOR} solid;
    `};
  ${props =>
    props.error &&
    css`
      border: 1px ${COLORS.LIGHT_RED} solid;
      margin-bottom: 0;
    `};
`;

const InputComponent = styled.input`
  font-size: 18px;
  font-weight: 100;
  font-family: 'TT Norms';
  width: 100%;

  :focus {
    outline: none;
  }
`;
const TextaAreaComponent = styled.textarea`
  height: 100px;
  max-height: 150px;
  min-height: 100px;
  font-size: 18px;
  font-weight: 100;
  font-family: 'TT Norms';
  min-width: 100%;

  :focus {
    outline: none;
  }
`;

const ANIMATION_DURATION_MILLISECONDS = 200;
const shakeAnimation = keyframes`
  20%, 80% {
    transform: translate3d(-2px, 0, 0);
  }
  40%, 60% {
    transform: translate3d(2px, 0, 0);
  }
`;

const Label = styled.p`
  color: ${COLORS.GRAY};
  ${props =>
    props.focused &&
    css`
      color: ${COLORS.MAIN_COLOR};
    `};
  ${props =>
    props.error &&
    css`
      color: ${COLORS.LIGHT_RED};
    `};
  ${props =>
    props.errorAnimation &&
    css`
      animation: ${shakeAnimation} ${ANIMATION_DURATION_MILLISECONDS}ms;
    `}
  font-weight: 400;
  margin-bottom: 5px;
  font-size: 16px;
`;

const ErrorContainer = styled.div`
  display: flex;
  height: 20px;
  align-items: center;
`;

const ErrorText = styled.p`
  text-transform: uppercase;
  color: ${COLORS.LIGHT_RED};
  font-size: 10px;
`;

export default class Input extends Component {
  static defaultProps = {
    placeholder: null,
    multiline: false,
    disabled: false,
    onBlur: () => false,
    type: 'text'
  };

  state = {
    errorAnimation: false,
    focused: false
  };

  startErrorAnimation = () => {
    this.setState({ errorAnimation: true });
    setTimeout(() => this.setState({ errorAnimation: false }), ANIMATION_DURATION_MILLISECONDS);
  };

  onBlur = () => {
    this.props.onBlur();
    this.setState({ focused: false });
  };

  render() {
    const { label, value, errorText, errorVisible, disabled, multiline, style } = this.props;
    const Input = multiline ? TextaAreaComponent : InputComponent;
    return (
      <Container style={style ? style : {}} disabled={disabled}>
        <Label errorAnimation={this.state.errorAnimation} focused={this.state.focused} error={errorVisible}>
          {label}
        </Label>
        <InputContainer multiline={multiline} focused={this.state.focused} error={errorVisible}>
          <Input
            ref={input => (this.input = input)}
            value={value}
            type={this.props.type}
            onSubmit={this.props.onSubmit}
            onChange={evt => this.props.onChange(evt.target.value)}
            onFocus={() => this.setState({ focused: true })}
            onBlur={this.onBlur}
            placeholder={this.props.placeholder}
          />
        </InputContainer>
        {errorVisible && (
          <ErrorContainer>
            <ErrorText>{errorText}</ErrorText>
          </ErrorContainer>
        )}
      </Container>
    );
  }
}
