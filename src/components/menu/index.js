import React, { Component } from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router';
import { MdClear } from 'react-icons/md';

const OuterContainer = styled.div`
  z-index: ${props => (props.visible ? 300 : -1)};
  position: fixed;
  height: 100vh;
  width: 100vw;
  display: flex;
  transition: 0.4s;
  top: 0;
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  background-color: ${props => (props.visible ? 'rgba(0, 0, 0, 0.5)' : 'transparent')};
`;

const InnerContainer = styled.div`
  box-sizing: border-box;
  position: relative;
  justify-content: flex-end;
  display: flex;
  flex-direction: column;
  background-color: white;
  padding-bottom: 25px;
  height: 250px;
  width: 100%;
  transition: 0.4s;
  transform: ${props => (props.visible ? 'translate(0, 0)' : 'translate(0, -280px)')};
`;

const CloseIconContainer = styled.div`
  user-select: none;
  cursor: pointer;
  height: 50px;
  width: 50px;
  border-radius: 25px;
  background-color: white;
  align-items: center;
  justify-content: center;
  display: flex;
  position: absolute;
  bottom: -20px;
  transform: translate(-25px, 0);
  left: 50%;
`;

const Link = styled.p`
  font-size: 18px;
  text-align: center;
  margin-bottom: 15px;
  user-select: none;
  cursor: pointer;
`;

class Menu extends Component {
  onItemClick = link => {
    this.props.history.push(link);
    this.props.close();
  };

  render() {
    const { visible, close } = this.props;

    return (
      <OuterContainer onClick={close} visible={visible}>
        <InnerContainer onClick={ev => ev.stopPropagation()} visible={visible}>
          <CloseIconContainer visible={visible} onClick={close}>
            <MdClear size={25} />
          </CloseIconContainer>
          <Link onClick={() => this.onItemClick('/categories-list')}>Категории</Link>
          <Link onClick={() => this.onItemClick('/products-list')}>Товары</Link>
          <Link onClick={() => this.onItemClick('/invoices-list')}>Накладные</Link>
          <Link onClick={() => this.onItemClick('/customers-list')}>Покупатели</Link>
          <Link onClick={() => this.onItemClick('/deliveries-list')}>Доставки</Link>
        </InnerContainer>
      </OuterContainer>
    );
  }
}

export default withRouter(Menu);
