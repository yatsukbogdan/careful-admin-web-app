import {
  INVOICE_STATUS_PENDING,
  INVOICE_STATUS_CANCELED,
  INVOICE_STATUS_PAID,
  INVOICE_STATUS_ALL,
  SENT_STATUS_ALL,
  SENT_STATUS_PENDING,
  SENT_STATUS_SENT
} from './helpers';

export const invoiceStatusOptions = [
  {
    value: INVOICE_STATUS_ALL,
    text: 'Все'
  },
  {
    value: INVOICE_STATUS_PENDING,
    text: 'В ожидании'
  },
  {
    value: INVOICE_STATUS_CANCELED,
    text: 'Отмененные'
  },
  {
    value: INVOICE_STATUS_PAID,
    text: 'Оплаченые'
  }
];

export const sentToCustomerStatusOptions = [
  {
    value: SENT_STATUS_ALL,
    text: 'Все'
  },
  {
    value: SENT_STATUS_PENDING,
    text: 'В ожидании отправки'
  },
  {
    value: SENT_STATUS_SENT,
    text: 'Отправленные'
  }
];
