export const deliveryAdressFromPayload = payload =>
  `${payload.streetName}, дом ${payload.house}, кв. ${payload.apartment}, поч. инд. ${payload.postcode}`;

export const customerStringFromPayload = payload =>
  `${payload.firstName}\n${payload.lastName}\n${payload.email}\n${payload.phone}`;

export const emptyInvoice = {
  paid: null,
  canceled: null,
  products: [],
  createDate: Date.now()
};

export const INVOICE_STATUS_ALL = 0;
export const INVOICE_STATUS_PENDING = 1;
export const INVOICE_STATUS_PAID = 2;
export const INVOICE_STATUS_CANCELED = 3;

export const SENT_STATUS_ALL = 0;
export const SENT_STATUS_PENDING = 1;
export const SENT_STATUS_SENT = 2;

export const deliverySentToCustomerStatusFromPayload = sentToCustomer => {
  if (sentToCustomer !== null) {
    return { id: SENT_STATUS_SENT, date: new Date(sentToCustomer).toLocaleString('uk-UA') };
  }
  return { id: SENT_STATUS_PENDING };
};

export const invoiceStatusFromPayload = (paid, canceled) => {
  if (paid !== null && (canceled === null || paid > canceled)) {
    return { id: INVOICE_STATUS_PAID, date: new Date(paid).toLocaleString('uk-UA') };
  }
  if (canceled !== null && (paid === null || canceled > paid)) {
    return { id: INVOICE_STATUS_CANCELED, date: new Date(canceled).toLocaleString('uk-UA') };
  }
  return { id: INVOICE_STATUS_PENDING };
};
