import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import { withRouter } from 'react-router-dom';
import API from '../../api';
import { addDeliveries } from '../../actions/deliveries';
import {
  deliveryAdressFromPayload,
  customerStringFromPayload,
  invoiceStatusFromPayload,
  INVOICE_STATUS_CANCELED,
  INVOICE_STATUS_PAID,
  INVOICE_STATUS_PENDING,
  deliverySentToCustomerStatusFromPayload,
  SENT_STATUS_ALL,
  INVOICE_STATUS_ALL,
  SENT_STATUS_PENDING,
  SENT_STATUS_SENT
} from './helpers';
import { numberToCurrency } from '../../utils';
import { COLORS } from '../../config';
import RadioGroup from '../../components/radioGroup';
import { invoiceStatusOptions, sentToCustomerStatusOptions } from './filterOptions';
import SentModal from './components/sentModal';

const Container = styled.div`
  position: relative;
`;

const FiltersContainer = styled.div`
  width: 200px;
  position: fixed;
  top: 0;
  box-sizing: border-box;
  padding: 15px;
  padding-top: 75px;
  height: 100vh;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

const DeliveriesContainer = styled.div`
  margin-left: 200px;
  overflow-y: scroll;
`;

const Label = styled.p`
  font-size: 25px;
  padding: 20px;
`;

const CustomerText = styled.p`
  white-space: pre;
`;

const Table = styled.table`
  width: 100%;
  padding: 10px;
  margin-bottom: 30px;
`;

const THeader = styled.th`
  width: 200px;
  text-align: left;
`;

const TCell = styled.th`
  padding: 5px;
  border: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
  flex: 1;
  text-align: left;
  font-weight: normal;
`;

const Button = styled.div`
  border: 2px ${COLORS.MAIN_COLOR} solid;
  padding: 5px;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  margin-top: 15px;
  width: 150px;
  user-select: none;
  ${props =>
    props.disabled &&
    css`
      filter: grayscale(50%);
      pointer-events: none;
      opacity: 0.3;
    `};
`;

const ButtonText = styled.p`
  margin-left: 10px;
`;

class DeliveriesListScreen extends Component {
  state = {
    error: null,
    loading: false,
    sentModalId: null,
    sentModalVisible: false,
    filters: {
      invoiceStatus: INVOICE_STATUS_ALL,
      sentToCustomerStatus: SENT_STATUS_ALL
    }
  };

  async componentDidMount() {
    const { data } = await API.getDeliveriesList();
    if (!data.success) return;
    this.props.addDeliveries({ deliveries: data.payload.deliveries });
  }

  _filterBySentToCustomerStatusMapFunction = delivery => {
    if (this.state.filters.sentToCustomerStatus === SENT_STATUS_ALL) return true;
    if (this.state.filters.sentToCustomerStatus === SENT_STATUS_PENDING)
      return (
        delivery.sentToCustomer.id === SENT_STATUS_PENDING && delivery.invoice.status.id !== INVOICE_STATUS_CANCELED
      );
    if (this.state.filters.sentToCustomerStatus === SENT_STATUS_SENT)
      return delivery.sentToCustomer.id === SENT_STATUS_SENT;
  };

  _filterByInvoiceStatusMapFunction = delivery => {
    if (this.state.filters.invoiceStatus === INVOICE_STATUS_ALL) return true;
    if (this.state.filters.invoiceStatus === INVOICE_STATUS_CANCELED)
      return delivery.invoice.status.id === INVOICE_STATUS_CANCELED;
    if (this.state.filters.invoiceStatus === INVOICE_STATUS_PAID)
      return delivery.invoice.status.id === INVOICE_STATUS_PAID;
    if (this.state.filters.invoiceStatus === INVOICE_STATUS_PENDING)
      return delivery.invoice.status.id === INVOICE_STATUS_PENDING;
  };

  get filteredDeliveries() {
    return this.props.deliveries
      .filter(this._filterByInvoiceStatusMapFunction)
      .filter(this._filterBySentToCustomerStatusMapFunction)
      .sort((d1, d2) => (d2.invoice && d1.invoice ? d2.invoice.createDate - d1.invoice.createDate : 0));
  }

  showSentModal = id => this.setState({ sentModalVisible: true, sentModalId: id });
  hideSentModal = () => this.setState({ sentModalVisible: false, sentModalId: null });

  onFilterChange = (filter, value) =>
    this.setState({
      filters: {
        ...this.state.filters,
        [filter]: value
      }
    });

  renderSentToCustomerStatus = delivery => {
    const {
      sentToCustomer,
      invoice: { status },
      declarationNumber,
      id
    } = delivery;
    if (status.id === INVOICE_STATUS_CANCELED) return null;
    if (sentToCustomer.id === SENT_STATUS_PENDING) {
      return (
        <Button onClick={() => this.showSentModal(id)}>
          <ButtonText>Отметить заказ как отправленый</ButtonText>
        </Button>
      );
    }
    return (
      <div>
        <p style={{ color: COLORS.YELLOW }}>Заказ отправлен {sentToCustomer.date}</p>
        <p style={{ color: COLORS.YELLOW }}>Номер декларации {declarationNumber}</p>
      </div>
    );
  };

  renderInvoiceStatus = status => {
    if (status.id === INVOICE_STATUS_CANCELED) {
      return <p style={{ color: COLORS.LIGHT_RED }}>Отменена {status.date}</p>;
    }
    if (status.id === INVOICE_STATUS_PAID) {
      return <p style={{ color: COLORS.MAIN_COLOR }}>Оплачена {status.date}</p>;
    }
    return false;
  };

  render() {
    return (
      <Container>
        <FiltersContainer>
          <RadioGroup
            label='Статус накладной'
            checked={this.state.filters.invoiceStatus}
            groupName='invoiceStatus'
            options={invoiceStatusOptions}
            onChange={value => this.onFilterChange('invoiceStatus', value)}
          />
          <RadioGroup
            label='Статус отправки'
            checked={this.state.filters.sentToCustomerStatus}
            groupName='sentToCustomerStatus'
            options={sentToCustomerStatusOptions}
            onChange={value => this.onFilterChange('sentToCustomerStatus', value)}
          />
        </FiltersContainer>
        <DeliveriesContainer>
          <Label>Доставки</Label>
          <Table>
            <tbody>
              <tr>
                <THeader>Покупатель</THeader>
                <THeader>Способ доставки</THeader>
                <THeader>Город</THeader>
                <THeader>Адрес</THeader>
                <THeader style={{ width: '600px' }}>Накладная</THeader>
              </tr>
              {this.filteredDeliveries.map(delivery => (
                <tr>
                  <TCell>
                    <CustomerText>{delivery.customer}</CustomerText>
                  </TCell>
                  <TCell>{delivery.method}</TCell>
                  <TCell>{delivery.cityName}</TCell>
                  <TCell>{delivery.adress}</TCell>
                  {delivery.invoice !== null && (
                    <TCell>
                      <p>
                        Дата создания:{' '}
                        <span style={{ fontWeight: 'bold' }}>
                          {new Date(delivery.invoice.createDate).toLocaleString()}
                        </span>
                      </p>
                      <Table>
                        <tbody>
                          <tr>
                            <THeader>Наименование</THeader>
                            <THeader>Количество (шт.)</THeader>
                            <THeader>Цена</THeader>
                            <THeader>Сума</THeader>
                          </tr>
                          {delivery.invoice.products.map((prod, index) => (
                            <tr key={index}>
                              <TCell>{prod.name}</TCell>
                              <TCell>{prod.quantity}</TCell>
                              <TCell>{numberToCurrency(Number(prod.price), 'UAH')}</TCell>
                              <TCell>{numberToCurrency(Number(prod.price) * Number(prod.quantity), 'UAH')}</TCell>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                      <p>
                        Общая сумма:{' '}
                        <span style={{ fontWeight: 'bold' }}>
                          {numberToCurrency(delivery.invoice.totalPrice, 'UAH')}
                        </span>
                      </p>
                      <p>Статус накладной:</p>
                      {this.renderInvoiceStatus(delivery.invoice.status)}
                      <p>Статус отправки:</p>
                      {this.renderSentToCustomerStatus(delivery)}
                    </TCell>
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
        </DeliveriesContainer>
        <SentModal
          deliveryId={this.state.sentModalId}
          onSentClick={() => this.changeSentStatus(this.state.sentModalId)}
          visible={this.state.sentModalVisible}
          close={this.hideSentModal}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  deliveries: Object.keys(state.deliveries).map(id => {
    const delivery = state.deliveries[id];
    const invoice = state.invoices[delivery.invoiceId];
    const customer = state.customers[delivery.customerId];
    return {
      id: delivery.id,
      cityName: delivery.cityName,
      method: delivery.method === 0 ? 'Новая почта' : 'Укрпочта',
      customer: customer ? customerStringFromPayload(customer) : 'customer',
      sentToCustomer: deliverySentToCustomerStatusFromPayload(delivery.sentToCustomer),
      declarationNumber: delivery.declarationNumber,
      adress:
        delivery.method === 0
          ? delivery.deliverySpecificData.nppName
          : deliveryAdressFromPayload(delivery.deliverySpecificData),
      invoice: invoice
        ? {
            ...invoice,
            products: invoice.products.map(product => ({
              name: state.products[product.id] ? state.products[product.id].name : 'unknown',
              quantity: product.quantity,
              price: product.price
            })),
            totalPrice: invoice.products.reduce((sum, prod) => sum + Number(prod.price) * Number(prod.quantity), 0),
            status: invoiceStatusFromPayload(invoice.paid, invoice.canceled)
          }
        : null
    };
  })
});

const mapDispatchToProps = {
  addDeliveries
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DeliveriesListScreen)
);
