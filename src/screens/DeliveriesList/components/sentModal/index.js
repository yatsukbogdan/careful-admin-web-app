import React, { Component } from 'react';
import { OuterContainer, SmallInnerContainer } from '../../../../components/instanceModal/styles';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Button from '../../../../components/button';
import { COLORS } from '../../../../config';
import Input from '../../../../components/input';
import API from '../../../../api';
import { updateDelivery } from '../../../../actions/deliveries';

const DestructionModalOuterContainer = styled(OuterContainer)`
  z-index: ${props => (props.visible ? 300 : 201)};
`;

const ButtonContainer = styled.div`
  display: flex;
`;

class SentModal extends Component {
  state = {
    input: '',
    inputError: false,
    inputErrorText: ''
  };

  onChange = input =>
    this.setState({
      input,
      inputError: false,
      inputErrorText: ''
    });

  validateInput = async () => {
    const isInputValid = this.isInputValid();
    await this.setState({
      inputError: !isInputValid,
      inputErrorText: 'Введите номер декларации'
    });
  };

  isInputValid = () => this.state.input.length > 0;

  onSentClick = async () => {
    await this.validateInput();
    if (this.state.inputError) {
      this.input.startErrorAnimation();
      return;
    }
    const { data } = await API.updateDeliverySentToCustomerStatus(this.props.deliveryId, {
      declarationNumber: this.state.input
    });
    if (!data.success) {
      alert(data.message);
    }
    this.props.updateDelivery({ id: data.payload.delivery.id, delivery: data.payload.delivery });
    this.close();
  };

  close = () => {
    this.props.close();
    this.onChange('');
  };

  onOuterContainerClick = ev => {
    this.close();
    ev.stopPropagation();
  };

  render() {
    return (
      <DestructionModalOuterContainer onClick={this.onOuterContainerClick} visible={this.props.visible}>
        <SmallInnerContainer onClick={ev => ev.stopPropagation()} visible={this.props.visible}>
          <Input
            value={this.state.input}
            label='Номер декларации'
            onChange={this.onChange}
            errorVisible={this.state.inputError}
            errorText={this.state.inputErrorText}
            onBlur={this.validateInput}
            ref={input => (this.input = input)}
          />
          <ButtonContainer>
            <Button
              label='Отметить заказ как отправленый'
              onClick={this.onSentClick}
              style={{ flex: 1, borderColor: COLORS.MAIN_COLOR, marginRight: '15px' }}
              textStyle={{ letterSpacing: 1, fontSize: '12px' }}
            />
            <Button
              label='Отмена'
              onClick={this.close}
              style={{ flex: 1, borderColor: COLORS.VERY_LIGHT_GRAY }}
              textStyle={{ letterSpacing: 1, fontSize: '14px' }}
            />
          </ButtonContainer>
        </SmallInnerContainer>
      </DestructionModalOuterContainer>
    );
  }
}

const mapDispatchToProps = {
  updateDelivery
};

export default connect(
  null,
  mapDispatchToProps
)(SentModal);
