import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  padding: 25px;
`;

const Link = styled.p`
  font-size: 18px;
  margin-bottom: 15px;
  user-select: none;
  cursor: pointer;
`;

export default class MainScreen extends Component {
  render() {
    return (
      <Container>
        <Link onClick={() => this.props.history.push('/categories-list')}>Категории</Link>
        <Link onClick={() => this.props.history.push('/products-list')}>Товары</Link>
        <Link onClick={() => this.props.history.push('/invoices-list')}>Накладные</Link>
        <Link onClick={() => this.props.history.push('/customers-list')}>Покупатели</Link>
        <Link onClick={() => this.props.history.push('/deliveries-list')}>Доставки</Link>
      </Container>
    );
  }
}
