import React, { Component } from 'react';
import styled from 'styled-components';
import md5 from 'js-md5';
import API from '../../api';
import { Spinner } from 'react-activity';
import Input from '../../components/input';
import { emailValidation } from '../../utils';
import Button from '../../components/button';
import { COLORS } from '../../config';

const PendingContainer = styled.div`
  z-index: 20;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(255, 255, 255, 0.7);
`;

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const InnerContainer = styled.div`
  width: 200px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const ErrorText = styled.p`
  color: red;
  font-size: 24px;
`;

const passwordValidation = pass => {
  if (pass.length === 0) return { success: false, error: 'Поле Пароль не может быть пустым' };
  return { success: true };
};

export default class LoginScreen extends Component {
  state = {
    email: '',
    emailError: false,
    emailErrorText: null,

    password: '',
    passwordError: '',
    passwordErrorText: '',

    error: null
  };

  onFieldChange = (value, field) =>
    this.setState({
      [field]: value,
      [`${field}Error`]: false,
      [`${field}ErrorText`]: '',
      error: null
    });

  login = async () => {
    this.setState({ error: null });
    const { data } = await API.login({
      email: this.state.email,
      passwordHash: md5(this.state.password)
    });
    if (!data.success) {
      this.setState({ error: data.message });
      return;
    }
    localStorage.setItem('token', data.payload.token);
    this.props.authorizeUser(data.payload.token);
  };

  validateField = async field => {
    let validationFunction;

    switch (field) {
      case 'email':
        validationFunction = emailValidation;
        break;
      case 'password':
        validationFunction = passwordValidation;
        break;
      default:
        validationFunction = emailValidation;
        break;
    }

    const validationObject = validationFunction(this.state[field]);

    if (validationObject.success) return;

    await this.setState({
      [`${field}Error`]: true,
      [`${field}ErrorText`]: validationObject.error
    });
  };

  validateAllFields = async () => {
    await Promise.all([this.validateField('email'), this.validateField('password')]);
  };

  onContinueClick = async () => {
    await this.validateAllFields();
    const { emailError, passwordError } = this.state;

    if (emailError || passwordError) {
      if (emailError) this.emailInput.startErrorAnimation();
      if (passwordError) this.passwordInput.startErrorAnimation();
      return;
    }
    this.login();
  };

  render() {
    return (
      <Container>
        {this.props.pending && (
          <PendingContainer>
            <Spinner size={30} color={COLORS.MAIN_COLOR} />
          </PendingContainer>
        )}
        <InnerContainer>
          <Input
            value={this.state.email}
            label='Email'
            onChange={value => this.onFieldChange(value, 'email')}
            onBlur={() => this.validateField('email')}
            ref={input => (this.emailInput = input)}
            errorVisible={this.state.emailError}
            errorText={this.state.emailErrorText}
          />
          <Input
            value={this.state.password}
            label='Password'
            type='password'
            onChange={value => this.onFieldChange(value, 'password')}
            onBlur={() => this.validateField('password')}
            ref={input => (this.passwordInput = input)}
            errorVisible={this.state.passwordError}
            errorText={this.state.passwordErrorText}
          />
          {this.state.error !== null && <ErrorText>{this.state.error}</ErrorText>}
          <Button onClick={this.onContinueClick} style={{ width: '200px' }} label='Войти' />
        </InnerContainer>
      </Container>
    );
  }
}
