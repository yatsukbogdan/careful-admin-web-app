import React, { Component } from 'react';
import styled from 'styled-components';
import InstanceItemInList from '../../../../../../components/instanceItemInList';

const Label = styled.p`
  font-size: 20px;
  margin-bottom: 10px;
  font-weight: bold;
`;

export default class Category extends Component {
  render() {
    return (
      <InstanceItemInList onDeleteClick={this.props.onDeleteClick} onEditClick={this.props.onEditClick}>
        <Label>{this.props.categoryName}</Label>
      </InstanceItemInList>
    );
  }
}
