import React, { Component } from 'react';
import styled from 'styled-components';
import Category from './components/category';

const Container = styled.div`
  padding: 50px;
`;

const Separator = styled.div`
  height: 50px;
  background-color: white;
`;

const Label = styled.p`
  font-size: 24px;
  letter-spacing: 1px;
  text-align: center;
  margin-bottom: 50px;
`;
export default class List extends Component {
  static defaultProps = {
    label: null
  };

  render() {
    const { categories, label } = this.props;

    return (
      <Container>
        {label !== null && <Label>{label}</Label>}
        {categories.map((category, index) => {
          if (index === 0) {
            return (
              <Category
                onDeleteClick={() => this.props.onDeleteClick(category.id)}
                onEditClick={() => this.props.onEditClick(category.id)}
                categoryName={category.name}
              />
            );
          }
          return (
            <div>
              <Separator />
              <Category
                onDeleteClick={() => this.props.onDeleteClick(category.id)}
                onEditClick={() => this.props.onEditClick(category.id)}
                categoryName={category.name}
              />
            </div>
          );
        })}
      </Container>
    );
  }
}
