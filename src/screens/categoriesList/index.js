import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import Button from '../../components/button';
import CategoryModal from '../../components/instanceModal/category';
import DestructionModal from '../../components/instanceModal/destructionModal';
import API from '../../api';
import { removeCategory, addCategories } from '../../actions/categories';
import List from './components/list';

const Container = styled.div`
  position: relative;
`;

class CategoriesListScreen extends Component {
  state = {
    error: null,
    loading: false,
    modal: {
      action: 'add',
      visible: false,
      id: null
    },
    deleteModal: {
      visible: false,
      id: null
    }
  };

  async componentDidMount() {
    const { data } = await API.getCategoriesList();
    if (!data.success) return;
    this.props.addCategories({ categories: data.payload.categories });
  }

  onAddCategoryClick = () =>
    this.setState({
      modal: {
        action: 'add',
        visible: true,
        id: null
      }
    });

  onEditCategoryClick = id =>
    this.setState({
      modal: {
        action: 'edit',
        visible: true,
        id
      }
    });

  onDeleteCategoryClick = id =>
    this.setState({
      deleteModal: {
        visible: true,
        id
      }
    });

  hideModal = () => this.setState({ modal: { ...this.state.modal, visible: false } });
  hideDestructionModal = () => this.setState({ deleteModal: { ...this.state.deleteModal, visible: false } });

  deleteCategory = async id => {
    this.hideModal();
    const { data } = await API.deleteCategory(id);
    if (!data.success) {
      alert(data.message);
      return;
    }

    this.props.removeCategory({ id });
  };

  renderDestructionModal = () => {
    const { categoriesObject } = this.props;
    const { id } = this.state.deleteModal;

    const categoryName = categoriesObject[id] ? categoriesObject[id].name : '';
    return (
      <DestructionModal
        destructionButtonText='Удалить'
        message={`Вы дейстивтельно хотите удалить категорию ${categoryName} ?`}
        visible={this.state.deleteModal.visible}
        onDeleteClick={() => this.deleteCategory(this.state.deleteModal.id)}
        close={this.hideDestructionModal}
      />
    );
  };

  render() {
    return (
      <Container>
        <Button
          style={{ width: '200px', marginLeft: '50px', marginTop: '100px' }}
          textStyle={{ fontSize: '12px', letterSpacing: 1 }}
          label='Добавить категорию'
          onClick={this.onAddCategoryClick}
        />
        <List
          onDeleteClick={this.onDeleteCategoryClick}
          onEditClick={this.onEditCategoryClick}
          categories={this.props.categories}
        />
        <CategoryModal
          onRemoveButtonClick={() => this.onDeleteCategoryClick(this.state.modal.id)}
          visible={this.state.modal.visible}
          close={this.hideModal}
          categoryId={this.state.modal.id}
          action={this.state.modal.action}
        />
        {this.renderDestructionModal()}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  categoriesObject: state.categories,
  categories: Object.keys(state.categories).map(id => state.categories[id])
});
const mapDispatchToProps = {
  removeCategory,
  addCategories
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CategoriesListScreen)
);
