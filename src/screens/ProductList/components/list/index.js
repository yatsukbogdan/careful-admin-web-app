import React, { Component } from 'react';
import Product from './components/product';
import styled from 'styled-components';

const Container = styled.div`
  padding: 50px;
`;

const Separator = styled.div`
  height: 50px;
  background-color: white;
`;

const Label = styled.p`
  font-size: 24px;
  letter-spacing: 1px;
  text-align: center;
  margin-bottom: 50px;
`;
export default class List extends Component {
  static defaultProps = {
    label: null
  };

  render() {
    const { products, label } = this.props;

    return (
      <Container>
        {label !== null && <Label>{label}</Label>}
        {products.map((product, index) => {
          if (index === 0) {
            return (
              <Product
                onToggleFeedbackVisibility={feedbackId => this.props.onToggleFeedbackVisibility(product.id, feedbackId)}
                onDeleteClick={() => this.props.onDeleteClick(product.id)}
                onEditClick={() => this.props.onEditClick(product.id)}
                product={product}
              />
            );
          }
          return (
            <div>
              <Separator />
              <Product
                onToggleFeedbackVisibility={feedbackId => this.props.onToggleFeedbackVisibility(product.id, feedbackId)}
                onDeleteClick={() => this.props.onDeleteClick(product.id)}
                onEditClick={() => this.props.onEditClick(product.id)}
                product={product}
              />
            </div>
          );
        })}
      </Container>
    );
  }
}
