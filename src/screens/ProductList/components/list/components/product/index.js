import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { MdVisibility, MdVisibilityOff, MdClear } from 'react-icons/md';
import { numberToCurrency } from '../../../../../../utils';
import { COLORS } from '../../../../../../config';
import InstanceItemInList from '../../../../../../components/instanceItemInList';

const AlbumContainer = styled.div`
  display: flex;
  overflow-x: scroll;
`;

const Image = styled.img`
  height: 120px;
  width: 120px;
`;

const Label = styled.p`
  font-size: 20px;
  margin-bottom: 10px;
`;

const LabelSpan = styled.span`
  font-size: 20px;
  font-weight: bold;
`;

const TextInfoContainer = styled.div`
  display: flex;
`;

const TextContainer = styled.div`
  padding: 15px;
  flex: 1;
`;

const Separator = styled.div`
  width: 1px;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

const FeedbacksContainer = styled.div`
  padding: 15px;
  flex-direction: column;
`;

const FeedbackContainer = styled.div`
  position: relative;
  padding: 15px;
  margin-bottom: 15px;
  border: 1px ${COLORS.LIGHT_GRAY} solid;
`;

const ActionButton = styled.div`
  height: 24px;
  width: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

const ButtonsContainer = styled.div`
  position: absolute;
  right: 10px;
  top: 10px;
  z-index: 9;
`;

const VisibilityButton = styled(ActionButton)`
  border: 2px ${COLORS.MAIN_COLOR} solid;
  margin-bottom: 10px;
  ${props =>
    !props.visible &&
    css`
      filter: grayscale(50%);
      opacity: 0.2;
    `};
`;

const RemoveButton = styled(ActionButton)`
  border: 2px ${COLORS.LIGHT_RED} solid;
`;

export default class Product extends Component {
  render() {
    const { product } = this.props;

    return (
      <InstanceItemInList onEditClick={this.props.onEditClick} onDeleteClick={this.props.onDeleteClick}>
        {product.album.length === 0 && <p>Фотографий нету</p>}
        <AlbumContainer>
          {product.album.map(albumObj => (
            <Image src={albumObj.url} />
          ))}
        </AlbumContainer>
        <TextInfoContainer>
          <TextContainer>
            <Label>
              ID: <LabelSpan>{product.id}</LabelSpan>
            </Label>
            <Label>
              Код товара: <LabelSpan>{product.code}</LabelSpan>
            </Label>
            <Label>
              Наименование: <LabelSpan>{product.name}</LabelSpan>
            </Label>
            <Label>
              Название на сайте: <LabelSpan>{product.siteName}</LabelSpan>
            </Label>
            <Label>
              Категория: <LabelSpan>{product.categoryName}</LabelSpan>
            </Label>
            <Label>
              Закупочная цена: <LabelSpan>{numberToCurrency(product.purchasePrice, product.currency)}</LabelSpan>
            </Label>
            <Label>
              Розничная цена: <LabelSpan>{numberToCurrency(product.retailPrice, product.currency)}</LabelSpan>
            </Label>
            <Label>
              Цена со скидкой:{' '}
              <LabelSpan>
                {product.discountPrice !== null
                  ? numberToCurrency(product.discountPrice, product.currency)
                  : 'Скидок нету'}
              </LabelSpan>
            </Label>
            <Label>
              Описание: <LabelSpan>{product.description}</LabelSpan>
            </Label>
          </TextContainer>
          <Separator />
          <TextContainer>
            <Label>
              Остаток на складе: <LabelSpan>{product.balance}</LabelSpan>
            </Label>
          </TextContainer>
        </TextInfoContainer>
        <FeedbacksContainer>
          {product.feedbacks.map(feedback => (
            <FeedbackContainer>
              <ButtonsContainer>
                <VisibilityButton
                  onClick={() => this.props.onToggleFeedbackVisibility(feedback.id)}
                  visible={feedback.visible}
                >
                  {feedback.visible ? (
                    <MdVisibility size={20} color={COLORS.MAIN_COLOR} />
                  ) : (
                    <MdVisibilityOff size={20} color={COLORS.MAIN_COLOR} />
                  )}
                </VisibilityButton>
                <RemoveButton>
                  <MdClear size={20} color={COLORS.LIGHT_RED} />
                </RemoveButton>
              </ButtonsContainer>
              <Label>
                ID: <LabelSpan>{feedback.id}</LabelSpan>
              </Label>
              <Label>
                CustomerID: <LabelSpan>{feedback.customerId}</LabelSpan>
              </Label>
              <Label>
                Имя: <LabelSpan>{feedback.name}</LabelSpan>
              </Label>
              <Label>
                Текст комментария: <LabelSpan>{feedback.text}</LabelSpan>
              </Label>
            </FeedbackContainer>
          ))}
        </FeedbacksContainer>
      </InstanceItemInList>
    );
  }
}
