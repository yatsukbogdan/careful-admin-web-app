import React, { Component } from 'react';
import List from './components/list';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import Button from '../../components/button';
import ProductModal from '../../components/instanceModal/product';
import DestructionModal from '../../components/instanceModal/destructionModal';
import API from '../../api';
import { removeProduct, addProducts, toggleProductFeedbackVisibility } from '../../actions/products';

const Container = styled.div`
  position: relative;
`;

class ProductListScreen extends Component {
  state = {
    error: null,
    loading: false,
    modal: {
      action: 'add',
      visible: false,
      id: null
    },
    deleteModal: {
      visible: false,
      id: null
    }
  };

  async componentDidMount() {
    const { data } = await API.getProductsList();
    if (!data.success) return;
    this.props.addProducts({ products: data.payload.products });
  }

  onAddProductClick = () =>
    this.setState({
      modal: {
        action: 'add',
        visible: true,
        id: null
      }
    });

  onEditProductClick = id =>
    this.setState({
      modal: {
        action: 'edit',
        visible: true,
        id
      }
    });

  onDeleteProductClick = id =>
    this.setState({
      deleteModal: {
        visible: true,
        id
      }
    });

  onToggleFeedbackVisibility = async (productId, feedbackId) => {
    const { data } = await API.toggleProductFeedbackVisibility(productId, feedbackId);
    if (!data.success) {
      alert(data.message);
      return;
    }
    this.props.toggleProductFeedbackVisibility({ productId, feedbackId });
  };

  hideModal = () => this.setState({ modal: { ...this.state.modal, visible: false } });
  hideDestructionModal = () => this.setState({ deleteModal: { ...this.state.deleteModal, visible: false } });

  deleteProduct = async id => {
    this.hideModal();
    const { data } = await API.deleteProduct(id);
    if (!data.success) {
      alert(data.message);
      return;
    }

    this.props.removeProduct({ id });
  };

  renderDestructionModal = () => {
    const { id } = this.state.deleteModal;
    const { productsObject } = this.props;
    const productName = productsObject[id] ? productsObject[id].name : '';
    return (
      <DestructionModal
        destructionButtonText='Удалить'
        message={`Вы действительно хотите удалить товар ${productName}`}
        visible={this.state.deleteModal.visible}
        onDeleteClick={() => this.deleteProduct(this.state.deleteModal.id)}
        close={this.hideDestructionModal}
      />
    );
  };

  render() {
    const { products } = this.props;

    return (
      <Container>
        <Button
          style={{ width: '200px', marginLeft: '50px', marginTop: '100px' }}
          textStyle={{ fontSize: '12px', letterSpacing: 1 }}
          label='Добавить товар'
          onClick={this.onAddProductClick}
        />
        <List
          onToggleFeedbackVisibility={this.onToggleFeedbackVisibility}
          onDeleteClick={this.onDeleteProductClick}
          onEditClick={this.onEditProductClick}
          products={products}
        />
        <ProductModal
          onRemoveButtonClick={() => this.onDeleteProductClick(this.state.modal.id)}
          visible={this.state.modal.visible}
          close={this.hideModal}
          productId={this.state.modal.id}
          action={this.state.modal.action}
        />
        {this.renderDestructionModal()}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  productsObject: state.products,
  products: Object.keys(state.products)
    .map(id => state.products[id])
    .map(prod => ({
      ...prod,
      categoryName: state.categories[prod.categoryId] != null ? state.categories[prod.categoryId].name : 'Unknown'
    }))
});

const mapDispatchToProps = {
  removeProduct,
  addProducts,
  toggleProductFeedbackVisibility
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProductListScreen)
);
