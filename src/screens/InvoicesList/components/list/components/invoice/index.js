import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { numberToCurrency } from '../../../../../../utils';
import { COLORS } from '../../../../../../config';
import { MdClear } from 'react-icons/md';

const Container = styled.div`
  padding: 10px;
  border: 2px ${COLORS.VERY_LIGHT_GRAY} solid;
  position: relative;
  min-height: 100px;
`;

const Label = styled.p`
  font-size: 20px;
  margin-bottom: 10px;
`;

const LabelSpan = styled.span`
  font-size: 20px;
  font-weight: bold;
`;

const SmallLabel = styled.p`
  font-size: 14px;
  margin-bottom: 10px;
`;

const SmallLabelSpan = styled.span`
  font-size: 14px;
  font-weight: bold;
`;

const Table = styled.table`
  width: 100%;
  border: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
  padding: 10px;
  margin-bottom: 30px;
`;

const THeader = styled.th`
  width: 200px;
  text-align: left;
`;

const TCell = styled.th`
  width: 200px;
  text-align: left;
  font-weight: normal;
`;

const Button = styled.div`
  border: 2px ${props => (props.type === 'cancel' ? COLORS.LIGHT_RED : COLORS.MAIN_COLOR)} solid;
  padding: 5px;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  width: 150px;
  margin-bottom: 10px;
  margin-left: auto;
  user-select: none;
  ${props =>
    props.disabled &&
    css`
      filter: grayscale(50%);
      pointer-events: none;
      opacity: 0.3;
    `};
`;

const ButtonText = styled.p`
  color: ${props => (props.type === 'cancel' ? COLORS.LIGHT_RED : COLORS.MAIN_COLOR)};
  margin-left: 10px;
`;

export default class Invoice extends Component {
  get typeLabel() {
    if (this.props.invoice.type === 1) {
      return 'Розничная';
    }
    return 'Приходная';
  }

  render() {
    return (
      <Container>
        <Label>
          Тип накладной: <LabelSpan>{this.typeLabel}</LabelSpan>
        </Label>
        <Label>
          Дата создания: <LabelSpan>{new Date(this.props.invoice.createDate).toLocaleString()}</LabelSpan>
        </Label>
        <Table>
          <tbody>
            <tr>
              <THeader>Наименование</THeader>
              <THeader>Количество (шт.)</THeader>
              <THeader>Цена</THeader>
              <THeader>Сума</THeader>
            </tr>
            {this.props.invoice.products.map((prod, index) => (
              <tr key={index}>
                <TCell>{prod.name}</TCell>
                <TCell>{prod.quantity}</TCell>
                <TCell>{numberToCurrency(Number(prod.price), 'UAH')}</TCell>
                <TCell>{numberToCurrency(Number(prod.price) * Number(prod.quantity), 'UAH')}</TCell>
              </tr>
            ))}
          </tbody>
        </Table>
        <Label>
          Общая сумма: <LabelSpan>{numberToCurrency(this.props.invoice.totalPrice, 'UAH')}</LabelSpan>
        </Label>
        {this.props.invoice.paid !== null && (
          <SmallLabel style={{ color: COLORS.MAIN_COLOR }}>
            Оплачено: <SmallLabelSpan>{this.props.invoice.paid}</SmallLabelSpan>
          </SmallLabel>
        )}
        {this.props.invoice.canceled !== null && (
          <SmallLabel style={{ color: COLORS.LIGHT_RED }}>
            Отменена: <SmallLabelSpan>{this.props.invoice.canceled}</SmallLabelSpan>
          </SmallLabel>
        )}
        <Button onClick={this.props.onCancelClick} type='cancel' disabled={this.props.invoice.canceled !== null}>
          <MdClear size={25} color={COLORS.LIGHT_RED} />
          <ButtonText type='cancel'>Отменить</ButtonText>
        </Button>
      </Container>
    );
  }
}
