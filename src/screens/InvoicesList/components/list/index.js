import React, { Component } from 'react';
import styled from 'styled-components';
import Invoice from './components/invoice';

const Container = styled.div`
  padding: 50px;
`;

const Separator = styled.div`
  height: 50px;
  background-color: white;
`;

const Label = styled.p`
  font-size: 24px;
  letter-spacing: 1px;
  text-align: center;
  margin-bottom: 50px;
`;
export default class List extends Component {
  static defaultProps = {
    label: null
  };

  render() {
    const { invoices, label } = this.props;
    return (
      <Container>
        {label !== null && <Label>{label}</Label>}
        {invoices.length === 0 && <p>Накладных нету</p>}
        {invoices.map((invoice, index) => {
          if (index === 0) {
            return (
              <Invoice
                key={index}
                onPaidClick={() => this.props.onPaidClick(invoice.id)}
                onCancelClick={() => this.props.onCancelClick(invoice.id)}
                invoice={invoice}
              />
            );
          }
          return (
            <div>
              <Separator />
              <Invoice
                key={index}
                onPaidClick={() => this.props.onPaidClick(invoice.id)}
                onCancelClick={() => this.props.onCancelClick(invoice.id)}
                invoice={invoice}
              />
            </div>
          );
        })}
      </Container>
    );
  }
}
