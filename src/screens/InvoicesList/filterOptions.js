export const invoiceTypesOptions = [
  {
    value: 0,
    text: 'Все'
  },
  {
    value: 1,
    text: 'Розничная'
  },
  {
    value: 2,
    text: 'Приходная'
  }
];

export const invoicePaidStatusOptions = [
  {
    value: 0,
    text: 'Все'
  },
  {
    value: 1,
    text: 'Оплаченые'
  },
  {
    value: 2,
    text: 'Неоплаченые'
  }
];

export const invoiceCanceledStatusOptions = [
  {
    value: 0,
    text: 'Все'
  },
  {
    value: 1,
    text: 'Отмененные'
  },
  {
    value: 2,
    text: 'Не отмененные'
  }
];
