import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import Button from '../../components/button';
import InvoiceModal from '../../components/instanceModal/invoice';
import API from '../../api';
import { removeInvoice, addInvoices, updateInvoice } from '../../actions/invoices';
import List from './components/list';
import DestructionModal from '../../components/instanceModal/destructionModal';
import { COLORS } from '../../config';
import RadioGroup from '../../components/radioGroup';
import { invoiceTypesOptions, invoiceCanceledStatusOptions, invoicePaidStatusOptions } from './filterOptions';

const Container = styled.div`
  position: relative;
`;

const FiltersContainer = styled.div`
  width: 200px;
  position: fixed;
  top: 0;
  box-sizing: border-box;
  padding: 15px;
  padding-top: 75px;
  height: 100vh;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

const InvoicesContainer = styled.div`
  margin-left: 200px;
`;

class InvoicesListScreen extends Component {
  state = {
    error: null,
    loading: false,
    filters: {
      invoiceType: 0,
      paidStatus: 0,
      canceledStatus: 0
    },
    modal: {
      action: 'add',
      visible: false,
      id: null
    },
    deleteModal: {
      visible: false,
      id: null
    }
  };

  async componentDidMount() {
    const { data } = await API.getInvoicesList();
    if (!data.success) return;
    this.props.addInvoices({ invoices: data.payload.invoices });
  }

  _filterByTypeMapFunction = invoice => {
    if (this.state.filters.invoiceType === 0) return true;
    if (this.state.filters.invoiceType === 1) return invoice.type === 1;
    if (this.state.filters.invoiceType === 2) return invoice.type === 2;
  };

  _filterByPaidStatusMapFunction = invoice => {
    if (this.state.filters.paidStatus === 0) return true;
    if (this.state.filters.paidStatus === 1) return invoice.paid !== null;
    if (this.state.filters.paidStatus === 2) return invoice.paid === null;
  };

  _filterByCanceledStatusMapFunction = invoice => {
    if (this.state.filters.canceledStatus === 0) return true;
    if (this.state.filters.canceledStatus === 1) return invoice.canceled !== null;
    if (this.state.filters.canceledStatus === 2) return invoice.canceled === null;
  };

  get filteredInvoices() {
    return this.props.invoices
      .filter(this._filterByTypeMapFunction)
      .filter(this._filterByPaidStatusMapFunction)
      .filter(this._filterByCanceledStatusMapFunction)
      .sort((i1, i2) => i2.createDate - i1.createDate);
  }

  onAddClick = () =>
    this.setState({
      modal: {
        action: 'add',
        visible: true,
        id: null
      }
    });

  onCancelClick = id =>
    this.setState({
      deleteModal: {
        visible: true,
        id
      }
    });

  cancelInvoice = async id => {
    const { data } = await API.cancelInvoice(id);
    this.onResponseHandler(id, data);
  };

  updateInvoicePaidStatusInvoice = async id => {
    const { data } = await API.updateInvoicePaidStatus(id);
    this.onResponseHandler(id, data);
  };

  onResponseHandler = (id, data) => {
    if (!data.success) {
      alert(data.message);
      return;
    }

    this.props.updateInvoice({ id, invoice: data.payload.invoice });
  };

  onFilterChange = (filter, value) =>
    this.setState({
      filters: {
        ...this.state.filters,
        [filter]: value
      }
    });

  hideModal = () => this.setState({ modal: { ...this.state.modal, visible: false } });
  hideDestructionModal = () => this.setState({ deleteModal: { ...this.state.deleteModal, visible: false } });

  render() {
    return (
      <Container>
        <FiltersContainer>
          <RadioGroup
            label='Тип'
            checked={this.state.filters.invoiceType}
            groupName='invoiceType'
            options={invoiceTypesOptions}
            onChange={value => this.onFilterChange('invoiceType', value)}
          />
          <RadioGroup
            label='Статус оплаты'
            checked={this.state.filters.paidStatus}
            groupName='paidStatus'
            options={invoicePaidStatusOptions}
            onChange={value => this.onFilterChange('paidStatus', value)}
          />
          <RadioGroup
            label='Статус отмены'
            checked={this.state.filters.canceledStatus}
            groupName='canceledStatus'
            options={invoiceCanceledStatusOptions}
            onChange={value => this.onFilterChange('canceledStatus', value)}
          />
        </FiltersContainer>
        <InvoicesContainer>
          <Button
            style={{ width: '200px', marginLeft: '50px', marginTop: '100px' }}
            textStyle={{ fontSize: '12px', letterSpacing: 1 }}
            label='Добавить накладную'
            onClick={this.onAddClick}
          />
          <List
            onPaidClick={this.updateInvoicePaidStatusInvoice}
            onCancelClick={this.onCancelClick}
            invoices={this.filteredInvoices}
          />
        </InvoicesContainer>
        <InvoiceModal
          visible={this.state.modal.visible}
          close={this.hideModal}
          invoiceId={this.state.modal.id}
          action={this.state.modal.action}
        />
        <DestructionModal
          destructionButtonText='Отменить накладную'
          message={`Вы действительно хотите отменить накладную ${this.state.deleteModal.id}?`}
          visible={this.state.deleteModal.visible}
          close={this.hideDestructionModal}
          onDeleteClick={() => this.cancelInvoice(this.state.deleteModal.id)}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  invoices: Object.keys(state.invoices).map(id => ({
    ...state.invoices[id],
    products: state.invoices[id].products.map(product => ({
      name: state.products[product.id] ? state.products[product.id].name : 'unknown',
      quantity: product.quantity,
      price: product.price
    })),
    totalPrice: state.invoices[id].products.reduce((sum, prod) => sum + Number(prod.price) * Number(prod.quantity), 0),
    paid: state.invoices[id].paid !== null ? new Date(state.invoices[id].paid).toLocaleString('uk-UA') : null,
    canceled: state.invoices[id].canceled !== null ? new Date(state.invoices[id].canceled).toLocaleString() : null
  }))
});

const mapDispatchToProps = {
  removeInvoice,
  updateInvoice,
  addInvoices
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(InvoicesListScreen)
);
