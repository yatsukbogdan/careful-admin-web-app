import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import API from '../../api';
import { addCustomers } from '../../actions/customers';

const Container = styled.div`
  position: relative;
`;

const Table = styled.table`
  width: 100%;
  padding: 10px;
  margin-bottom: 30px;
`;

const THeader = styled.th`
  width: 200px;
  text-align: left;
`;

const TCell = styled.th`
  flex: 1;
  text-align: left;
  font-weight: normal;
`;

class CustomersListScreen extends Component {
  state = {
    error: null,
    loading: false
  };

  async componentDidMount() {
    const { data } = await API.getCustomersList();
    if (!data.success) return;
    this.props.addCustomers({ customers: data.payload.customers });
  }

  render() {
    return (
      <Container>
        <Table>
          <tbody>
            <tr>
              <THeader>Имя</THeader>
              <THeader>Фамилия</THeader>
              <THeader>E-mail</THeader>
              <THeader>Номер телефона</THeader>
            </tr>
            {this.props.customers.map((customer, index) => (
              <tr key={index}>
                <TCell>{customer.firstName}</TCell>
                <TCell>{customer.lastName}</TCell>
                <TCell>{customer.email}</TCell>
                <TCell>{customer.phone}</TCell>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  customers: Object.keys(state.customers).map(id => state.customers[id])
});

const mapDispatchToProps = {
  addCustomers
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CustomersListScreen)
);
