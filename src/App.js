import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import Header from './components/header';
import ProductsListScreen from './screens/ProductList';
import CategoriesListScreen from './screens/CategoriesList';
import InvoicesListScreen from './screens/InvoicesList';
import CustomersListScreen from './screens/CustomersList';
import DeliveriesListScreen from './screens/DeliveriesList';
import MainScreen from './screens/Main';
import store from './store';
import ScrollToTop from './components/scrollToTop';
import Menu from './components/menu';
import LoginScreen from './screens/Login';
import API from './api';

import { addCategories } from './actions/categories';
import { addProducts } from './actions/products';
import { addInvoices } from './actions/invoices';
import { addDeliveries } from './actions/deliveries';
import { addCustomers } from './actions/customers';

class AppRouter extends Component {
  state = {
    token: null,
    loaded: {
      categories: false,
      products: false,
      invoices: false,
      customers: false,
      deliveries: false
    },
    pending: {
      categories: false,
      products: false,
      invoices: false,
      customers: false,
      deliveries: false
    },
    menuVisible: false,
    instanceModal: {
      visible: false,
      action: 'add',
      id: null
    }
  };

  componentDidMount() {
    if (localStorage.token == null) return;

    this.onLoginSuccess(localStorage.token);
  }

  get isAppVisible() {
    return this.state.token !== null && this.isAllInstancesLoaded;
  }

  get isAllInstancesLoaded() {
    const { loaded } = this.state;
    return loaded.categories && loaded.products && loaded.customers && loaded.deliveries && loaded.invoices;
  }

  get isPending() {
    const { pending } = this.state;
    return pending.categories || pending.products || pending.customers || pending.deliveries || pending.invoices;
  }

  onLoginSuccess = token => {
    this.authorizeUser(token);
    this.loadAppData();
  };

  authorizeUser = token => {
    API.patch(token);
    this.setState({ token });
  };

  loadAppData = () => {
    this.getCategories();
    this.getProducts();
    this.getInvoices();
    this.getDeliveries();
    this.getCustomers();
  };

  setInstancePendingValue = (instance, value) => {
    this.setState({ pending: { ...this.state.pending, [instance]: value } });
  };

  setInstanceLoadedValue = (instance, value) => {
    this.setState({ loaded: { ...this.state.loaded, [instance]: value } });
  };

  loadInstance = async (field, request, action) => {
    this.setInstancePendingValue(field, true);
    const { data } = await request();
    this.setInstancePendingValue(field, false);
    this.setInstanceLoadedValue(field, true);
    if (!data.success) return;
    store.dispatch(action({ [field]: data.payload[field] }));
  };

  getCategories = async () => await this.loadInstance('categories', () => API.getCategoriesList(), addCategories);
  getProducts = async () => await this.loadInstance('products', () => API.getProductsList(), addProducts);
  getInvoices = async () => await this.loadInstance('invoices', () => API.getInvoicesList(), addInvoices);
  getDeliveries = async () => await this.loadInstance('deliveries', () => API.getDeliveriesList(), addDeliveries);
  getCustomers = async () => await this.loadInstance('customers', () => API.getCustomersList(), addCustomers);

  logout = () => {
    API.logout();
    this.setState({ token: null });
    localStorage.removeItem('token');
  };

  showMenu = () => this.setState({ menuVisible: true });
  hideMenu = () => this.setState({ menuVisible: false });

  renderApp = () => {
    if (!this.isAppVisible) {
      return <LoginScreen pending={this.isPending} authorizeUser={this.onLoginSuccess} />;
    }
    return (
      <div>
        <Header onMenuClick={this.showMenu} onCartClick={this.showCartModal} onLoopClick={this.showSearchBar} />
        <Menu close={this.hideMenu} visible={this.state.menuVisible} />
        <div style={{ marginTop: '60px' }}>
          <Route path='/' exact component={MainScreen} />
          <Route path='/products-list' exact component={ProductsListScreen} />
          <Route path='/categories-list' exact component={CategoriesListScreen} />
          <Route path='/invoices-list' exact component={InvoicesListScreen} />
          <Route path='/customers-list' exact component={CustomersListScreen} />
          <Route path='/deliveries-list' exact component={DeliveriesListScreen} />
        </div>
        <ScrollToTop />
      </div>
    );
  };

  render() {
    return (
      <Provider store={store}>
        <Router onUpdate={() => window.scrollTo(0, 0)}>{this.renderApp()}</Router>
      </Provider>
    );
  }
}

export default AppRouter;
